<?php

Route::group(['middleware' => 'auth'], function () {
    Route::get('/chat', 'ForumController@index')->name('show.forum');
    Route::get('/chat/create', 'ForumController@create')->name('create.forum');
    Route::get('/chat/{post}/comments', 'ForumController@showComments')->name('show.comments');
    Route::get('/reports/{parcel}/download', 'ParcelController@download')->name('parcel.download');
    Route::get('/parcels/{parcel}/download/all', 'ParcelController@downloadAll')->name('parcel.download_all');
    Route::get('/my/reports/add', 'ParcelController@create')->name('parcel.create');
    Route::get('/my/reports/{parcel}/media', 'ParcelController@media')->name('parcel.media');
    Route::get('/my/reports', 'ParcelController@index')->name('parcel.index');
    Route::get('/my/reports/{parcel}/edit', 'ParcelController@edit')->name('parcel.edit');
    Route::put('/my/reports/{parcel}', 'ParcelController@update')->name('parcel.update');
    Route::delete('/my/reports/{parcel}', 'ParcelController@delete')->name('parcel.delete');
    Route::post('/my/reports', 'ParcelController@store')->name('parcel.store');
    Route::get('/my/profile', 'Auth\ProfileController@edit')->name('profile.edit');
    Route::post('/my/profile', 'Auth\ProfileController@update')->name('profile.update');
    Route::get('/my/wishlist', 'Auth\TransactionController@getWishlist')->name('my.wishlist');
    Route::get('/my/purchases', 'Auth\TransactionController@getPurchases')->name('my.purchases');
    Route::get('/my/subscription', 'Auth\SubscriptionController@index')->name('my.subscription');
    Route::post('/my/reviews/add', 'ReviewController@store')->name('review.store');
    Route::delete('/my/reviews/{review}', 'ReviewController@delete')->name('review.delete');
    Route::put('/my/reviews/{review}', 'ReviewController@update')->name('review.update');
    Route::get('/my/bonus', 'Auth\BonusController@index')->name('my.bonus');
    Route::delete('/my/bonus', 'Auth\BonusController@subtract')->name('my.bonus.subtract');
    Route::get('/my/viewed', 'ParcelController@myViewed')->name('my.viewed.parcels');
    Route::get('/my/notifications', 'HomeController@notifications')->name('my.notifications');
    Route::post('/my/notifications', 'HomeController@markAsRead');
    Route::post('/my/subscriptions/webpush', 'Auth\NotificationController@webpush');
    Route::delete('/my/subscriptions/webpush', 'Auth\NotificationController@destroy');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'api'], function () {
    Route::post('/parcel/{parcel}/upload', 'Api\FrontController@uploadMedia')->name('parcel.media.upload');
    Route::post('/parcels/{parcel}/vote', 'Api\FrontController@vote')->name('parcel.vote');
    Route::delete('/parcel/{parcel}/media/{media}', 'Api\FrontController@removeMedia')->name('parcel.media.remove');
    Route::post('/create-payment', 'Api\PayPalController@createPayment')->name('paypal.payment.create');
    Route::post('/execute-payment/{paymentID}/{payerID}', 'Api\PayPalController@executePayment')->name('paypal.payment.execute');
    Route::get('/chat/suppliers', 'Api\PostController@getSuppliers')->name('api.forum.suppliers');
    Route::post('/chat/{post}/comments', 'Api\PostController@addComment')->name('add.comment');
    Route::put('/chat/{post}/comments/{comment}', 'Api\PostController@editComment')->name('edit.comment');
    Route::delete('/chat/{post}/comments/{comment}', 'Api\PostController@deleteComment')->name('delete.comment');
    Route::post('/chat/{post}/vote', 'Api\PostController@votePost')->name('forum.vote');
    Route::delete('/chat/{post}', 'Api\PostController@deletePost')->name('forum.delete');
    Route::post('/chat', 'Api\PostController@addPost')->name('store.forum');
    Route::put('/chat/{post}', 'Api\PostController@editPost')->name('forum.edit');
    Route::get('/my/points', 'Auth\BonusController@points')->name('my.points');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'api/my'], function () {
    Route::post('wishlist/{thread}', 'Api\My\TransactionController@addOrRemoveWishlist');
    Route::post('purchases', 'Api\My\TransactionController@makePurchases')->name('make.purchases');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('/suppliers', 'Api\FrontController@getSuppliers')->name('suppliers.search');
    Route::get('/tenders', 'Api\FrontController@getTenders')->name('tenders.search');
    Route::get('/search', 'Api\FrontController@search')->name('parcel.search');
    Route::get('/search/thread/{parcel}', 'Api\FrontController@getThread')->name('parcel.get.thread');
    Route::get('/parcel/reports/', 'ParcelController@canAdd');
});

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/admin/tenders', 'Admin\TenderController@index')->name('admin.tender.index');
    Route::get('/admin/tenders/create', 'Admin\TenderController@create')->name('admin.tender.create');
    Route::get('/admin/tenders/{tender}', 'Admin\TenderController@edit')->name('admin.tender.edit');
    Route::get('/admin/users', 'Admin\UserController@index')->name('admin.user.index');
    Route::get('/admin/reports', 'Admin\ReportController@index')->name('admin.report.index');
    Route::delete('/admin/reports/{parcel}', 'Admin\ReportController@delete')->name('admin.report.delete');
    Route::put('/admin/tenders/{tender}', 'Admin\TenderController@update')->name('admin.tender.update');
    Route::post('/admin/tenders', 'Admin\TenderController@store')->name('admin.tender.store');

    Route::get('/admin/suppliers', 'Admin\SupplierController@index')->name('admin.supplier.index');
    Route::get('/admin/suppliers/create', 'Admin\SupplierController@create')->name('admin.supplier.create');
    Route::get('/admin/suppliers/{supplier}', 'Admin\SupplierController@edit')->name('admin.supplier.edit');
    Route::put('/admin/suppliers/{supplier}', 'Admin\SupplierController@update')->name('admin.supplier.update');
    Route::post('/admin/suppliers', 'Admin\SupplierController@store')->name('admin.supplier.store');
    Route::get('/admin/ads', 'Admin\AdsController@index')->name('admin.ads.index');
    Route::get('/admin/ads/edit/{ads}', 'Admin\AdsController@edit')->name('admin.ads.edit');
    Route::put('/admin/ads/{ads}', 'Admin\AdsController@update')->name('admin.ads.update');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::post('/register/email', 'Auth\RegisterController@byEmail')->name('register.email');
Route::post('/register/email/send', 'Auth\RegisterController@sendCode');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/page/{name}', 'PageController@index')->name('page');
Route::get('/page/{name}', 'PageController@index')->name('page');
Route::get('/parcels/{parcel}/{thread}', 'ParcelController@show')->name('parcel.show');
Route::get('/tenders', 'TenderController@index')->name('tender.index');
Route::get('/tenders/{tender}', 'TenderController@show')->name('tender.show');
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')->name('auth.provider');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('auth.callback');
Route::post('/login/email/{token}', 'Auth\LoginController@verifyEmail')->name('verify.email');
Route::get('/speed', 'HomeController@speed');
