<?php
namespace App;

use App\Models\Parcel;
use App\Models\Rate;
use App\Models\Review;
use App\Models\Transaction;

class Repository
{
    public function __construct()
    {

    }

    public function reportRates($parceId, $value = 1)
    {
        return Rate::selectRaw("COALESCE(sum(value), 0) as sum, count(value) as count")
            ->whereSubjectableId($parceId)
            ->whereSubjectableType(Parcel::class)
            ->whereValue($value)
            ->first();
    }

    public function reportUserRates($userId, $value = 1)
    {
        return Rate::selectRaw("COALESCE(sum(value), 0) as sum, count(value) as count")
            ->whereOwnerId($userId)
            ->whereSubjectableType(Parcel::class)
            ->whereValue($value)
            ->first();
    }

    public function parcelStarts($ids)
    {
        if (is_string($ids)) {
            $ids = $this->parcelIds($ids);
        }

        return Review::whereIn('subjectable_id', $ids)
            ->selectRaw('avg(rate) as avg, count(*) as count')
            ->first()
            ->toArray();
    }

    public function parcelIds(string $thread)
    {
        return Parcel::where('thread', $thread)
            ->select('id')
            ->get()
            ->pluck('id', 'id');
    }

    public function lastReportIds($tender)
    {
        // Get last report in parcel for giving tender
        return Parcel::selectRaw('max(id) as id')
            ->live()
            ->whereTenderId($tender)
            ->groupBy(['parcel_number', 'tender_id'])
            ->get()
            ->toArray();
    }

    public function mostDownload($limit = 8)
    {
        return Parcel::selectRaw("max(download), parcels.*")
            ->with([
                'media',
                'tender' => function ($q) {
                    return $q->select('title', 'id');
                }])
            ->live()
            ->groupBy('thread')
            ->limit($limit)
            ->get();
    }

    public function getWishlist($userId)
    {
        $threads = Transaction::where('in_wishlist', true)
            ->where('user_id', $userId)
            ->select('thread')
            ->get()
            ->pluck('thread', 'thread');

        return Parcel::whereIn('thread', $threads)
            ->paginate(10);
    }

    public function getPurchases($userId)
    {
        $threads = Transaction::where('done', true)
            ->where('user_id', $userId)
            ->select('thread')
            ->get()
            ->pluck('thread', 'thread');

        return Parcel::whereIn('thread', $threads)
            ->paginate(10);
    }
}
