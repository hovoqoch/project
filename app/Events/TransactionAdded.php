<?php

namespace App\Events;

use App\Models\User;

class TransactionAdded
{
    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        $this->type = $type;

        $this->user = User::first();
    }
}
