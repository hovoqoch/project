<?php

namespace App\Events;

use App\Models\Parcel;
use Illuminate\Queue\SerializesModels;

class ReportDeleted
{
    use SerializesModels;

    /**
     *
     * @var \App\Models\Parcel
     */
    public $parcel;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Parcel $parcel)
    {
        $this->parcel = $parcel;
    }
}
