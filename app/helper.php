<?php

use App\Services\Settings;

function settings(string $type)
{
    return (new Settings())->get($type);
}

function stars(?int $value = 0)
{
    $stars = [];
    for ($i = 1; $i <= 5; $i++) {
        $stars[] = sprintf("<i class='fa fa-star %s'></i>",
            ($value >= $i) ? 'fa-star--checked' : 'fa-star--unchecked'
        );
    }
    return implode($stars);
}

function appPackages()
{
    return [
        'month' => ['price' => '500', 'points' => '500', 'month' => 1, 'limit' => 10],
        'thread' => ['price' => '50', 'points' => '50', 'month' => 0, 'limit' => 1],
        'tender' => ['price' => '250', 'points' => '250', 'month' => 0, 'limit' => 5],
        'points' => [
            ['price' => 200, 'text' => "Buy 250 points with $200", 'points' => 250],
            ['price' => 400, 'text' => "Buy 600 points with $400", 'points' => 600],
            ['price' => 600, 'text' => "Buy 800 points with $600", 'points' => 800],
        ],
    ];
}

function prices()
{
    return [
        'parcel' => 50,
        'tender' => 200,
        'all' => 500,
    ];
}

function floTypes()
{
    return [
        ['image' => asset('/images/flo/none.png'), 'url' => '/search?status=active&flourscene=None', 'title' => 'None'],
        ['image' => asset('/images/flo/faint.png'), 'url' => '/search?status=active&flourscene=Faint', 'title' => 'Faint'],
        ['image' => asset('/images/flo/medium.png'), 'url' => '/search?status=active&flourscene=Medium', 'title' => 'Medium'],
        ['image' => asset('/images/flo/strong.png'), 'url' => '/search?status=active&flourscene=Strong', 'title' => 'Strong'],
        ['image' => asset('/images/flo/vstrong.png'), 'url' => '/search?status=active&flourscene=Very%20Strong', 'title' => 'Very Strong'],
    ];
}

function colorTypes($type = 'color_by_machine')
{
    $url = '/search?status=active&' . $type;
    return [
        ['id' => 'D', 'title' => 'D', 'image' => '', 'url' => $url . '=' . 'D'],
        ['id' => 'E', 'title' => 'E', 'image' => '', 'url' => $url . '=' . 'E'],
        ['id' => 'F', 'title' => 'F', 'image' => '', 'url' => $url . '=' . 'F'],
        ['id' => 'G', 'title' => 'G', 'image' => '', 'url' => $url . '=' . 'G'],
        ['id' => 'H', 'title' => 'H', 'image' => '', 'url' => $url . '=' . 'H'],
        ['id' => 'I', 'title' => 'I', 'image' => '', 'url' => $url . '=' . 'I'],
        ['id' => 'J', 'title' => 'J', 'image' => '', 'url' => $url . '=' . 'J'],
        ['id' => 'K', 'title' => 'K', 'image' => '', 'url' => $url . '=' . 'K'],
        ['id' => 'L', 'title' => 'L', 'image' => '', 'url' => $url . '=' . 'L'],
        ['id' => 'M', 'title' => 'M', 'image' => '', 'url' => $url . '=' . 'M'],
        ['id' => 'Brown (light)', 'title' => 'Brown (light)', 'image' => '', 'url' => $url . '=' . 'Brown (light)'],
        ['id' => 'Brown (Medium)', 'title' => 'Brown (Medium)', 'image' => '', 'url' => $url . '=' . 'Brown (Medium)'],
        ['id' => 'Brown (Dark)', 'title' => 'Brown (Dark)', 'image' => '', 'url' => $url . '=' . 'Brown (Dark)'],
        ['id' => 'Fancy yellow (Light fancy)', 'title' => 'Fancy yellow (Light fancy)', 'image' => '', 'url' => $url . '=' . 'Fancy yellow (Light fancy)'],
        ['id' => 'Fancy yellow (Fancy)', 'title' => 'Fancy yellow (Fancy)', 'image' => '', 'url' => $url . '=' . 'Fancy yellow (Fancy)'],
        ['id' => 'Fancy yellow (Intense)', 'title' => 'Fancy yellow (Intense)', 'image' => '', 'url' => $url . '=' . 'Fancy yellow (Intense)'],
        ['id' => 'Fancy yellow (Vivid)', 'title' => 'Fancy yellow (Vivid)', 'image' => '', 'url' => $url . '=' . 'Fancy yellow (Vivid)'],
        ['id' => 'Blue (Light fancy)', 'title' => 'Blue (Light fancy)', 'image' => '', 'url' => $url . '=' . 'Blue (Light fancy)'],
        ['id' => 'Blue (Fancy)', 'title' => 'Blue (Fancy)', 'image' => '', 'url' => $url . '=' . 'Blue (Fancy)'],
        ['id' => 'Blue (Intense)', 'title' => 'Blue (Intense)', 'image' => '', 'url' => $url . '=' . 'Blue (Intense)'],
        ['id' => 'Blue (Vivid)', 'title' => 'Blue (Vivid)', 'image' => '', 'url' => $url . '=' . 'Blue (Vivid)'],
        ['id' => 'Pink (Light fancy)', 'title' => 'Pink (Light fancy)', 'image' => '', 'url' => $url . '=' . 'Pink (Light fancy)'],
        ['id' => 'Pink (Fancy)', 'title' => 'Pink (Fancy)', 'image' => '', 'url' => $url . '=' . 'Pink (Fancy)'],
        ['id' => 'Pink (Intense)', 'title' => 'Pink (Intense)', 'image' => '', 'url' => $url . '=' . 'Pink (Intense)'],
        ['id' => 'Pink (Vivid)', 'title' => 'Pink (Vivid)', 'image' => '', 'url' => $url . '=' . 'Pink (Vivid)'],
    ];
}

function sizeTypes()
{
    return [
        ['title' => '1', 'image' => '', 'url' => '/search?status=active&size_from=0&size_to=1'],
        ['title' => '2', 'image' => '', 'url' => '/search?status=active&size_from=1&size_to=2'],
        ['title' => '3', 'image' => '', 'url' => '/search?status=active&size_from=2&size_to=3'],
        ['title' => '5', 'image' => '', 'url' => '/search?status=active&size_from=3&size_to=5'],
        ['title' => '10', 'image' => '', 'url' => '/search?status=active&size_from=5&size_to=10'],
    ];
};
