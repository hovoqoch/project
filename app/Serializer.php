<?php

namespace App;

use App\Models\Parcel;
use App\Models\Tender;
use App\Models\User;
use App\Repositories\ForumRespository;
use App\Repository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class Serializer
{
    public function parcel($parcels)
    {
        $data = [];
        $repo = new Repository;

        foreach ($parcels as $parcel) {
            $meta = $this->meta($parcel->thread, $parcel);

            $data[] = [
                'meta' => $meta,
                'content' => $this->parcelContent($parcel, $meta),
            ];
        }

        return $data;
    }

    public function canDownloadTender(Tender $tender)
    {
        $user = Auth::user();
        if (!$user) {
            return false;
        }
        $hasActiveSubscribe = $user->hasActiveSubscribe();
        $tenders = $user->tenderCanDownload();

        return ($hasActiveSubscribe || Arr::has($tenders, $tender->id));
    }

    public function meta(string $thread, Parcel $parcel)
    {
        $repo = new Repository;
        $ids = $repo->parcelIds($thread);
        $rates = $repo->parcelStarts($ids);

        $meta = [
            'hasActiveSubscribe' => false,
            'purchases' => [],
            'isAuth' => false,
            'wishList' => [],
            'tenders' => [],
            'inTenders' => false,
            'inWishList' => false,
            'showPrice' => true,
            'price' => '$' . prices()['parcel'],
            'boughtIt' => false,
            'user' => Auth::user(),
            'reportIds' => $ids,
            'randomParcelId' => $ids->first(),
            'rates' => [
                'avg' => number_format($rates['avg'], 2),
                'count' => $rates['count'],
            ],
            'thread' => $thread,
            'stars' => stars($rates['avg']),
            'url' => route('parcel.show', [$parcel->id, $thread]),
            'download' => route('parcel.download_all', $parcel->id),
            'reportText' => trans_choice('text.reports', count($ids), ['count' => count($ids)]),
            'payment' => [],
        ];

        if ($meta['user']) {
            $hasActiveSubscribe = $meta['user']->hasActiveSubscribe();
            $purchases = $meta['user']->buy();
            $tenders = $meta['user']->tenderCanDownload();

            $meta['hasActiveSubscribe'] = $hasActiveSubscribe;
            $meta['purchases'] = $purchases;
            $meta['wishList'] = $meta['user']->wishList();
            $meta['showPrice'] = ($hasActiveSubscribe || Arr::has($purchases, $thread)) ? false : true;
            $meta['boughtIt'] = $hasActiveSubscribe || Arr::has($purchases, $thread) || Arr::has($tenders, $parcel->tender_id);
            $meta['inWishList'] = Arr::has($meta['wishList'], $thread) ? true : false;
            $meta['isAuth'] = true;
            $meta['tenders'] = $tenders;
            $meta['inTenders'] = Arr::has($tenders, $parcel->tender_id);
            $meta['payment'] = [
                'payload' => [
                    'thread' => $thread,
                    'action' => 'buyThread',
                    'points' => appPackages()['thread']['price'],
                ],
                'paypal' => [
                    'description' => "Buy all reports # $thread",
                    'amount' => [
                        'currency_code' => 'USD',
                        'value' => appPackages()['thread']['price'],
                    ],
                ],
            ];
        }

        return $meta;
    }

    public function mostDownload($reports)
    {
        $data = [];
        foreach ($reports as $report) {
            $data[] = [
                'image' => $report->getImageUrl(),
                'id' => $report->id,
                'rates' => (new Repository)->parcelStarts($report->thread),
                'thread' => $report->thread,
                'number' => $report->parcel_number,
                'tender' => $report->tender->title,
                'url' => route('parcel.show', [$report->id, $report->thread]),
            ];
        }

        return $data;
    }

    public function parcelContent(Parcel $parcel, array $meta)
    {
        $show = false;
        $repo = new Repository;
        if (
            ($meta['user'] && $meta['user']->id == $parcel->user_id) ||
            ($meta['user'] && $meta['boughtIt'])
        ) {
            $show = true;
        }

        return [
            'id' => $parcel->id,
            'tenderId' => $parcel->tender_id,
            'img' => $parcel->getImageUrl(),
            'slider' => $parcel->getSlider(),
            'parcel_number' => $parcel->parcel_number,
            'color_by_eye' => $show ? $parcel->color_by_eye : '.',
            'color_by_machine' => $show ? $parcel->color_by_machine : '.',
            'size' => $show ? $parcel->size : '.',
            'flourscene' => $show ? $parcel->flourscene : '.',
            'valuation_from' => $show ? $parcel->valuation_from : '.',
            'valuation_to' => $show ? $parcel->valuation_to : '.',
            'hasIt' => $meta['user'] && $meta['user']->id == $parcel->user_id,
            'like' => $repo->reportRates($parcel->id),
            'dislike' => $repo->reportRates($parcel->id, -1),
        ];
    }

    public function canSeeIt(Parcel $report, ?User $user, string $content, bool $boughtIt)
    {
        if ($user && $user->id == $report->user_id) {
            return $report->$content;
        }
        if ($boughtIt) {
            return $report->$content;
        }

        return '.';
    }

    public function hasIt(?User $user, Parcel $report)
    {
        if (!$user) {
            return false;
        }
        if ($user->id == $report->user_id) {
            return true;
        }

        return false;
    }

    public function ourTender($tenders)
    {
        $data = [];
        foreach ($tenders as $tender) {
            $hasImage = $tender->hasImage();
            $data[] = [
                'status' => $tender->formatStatus(),
                'hasImage' => $hasImage,
                'image' => $hasImage ? $tender->getImageUrl() : '/images/tender.png',
                'url' => route('tender.show', $tender->id),
                'title' => $tender->title,
                'count' => $tender->parcelsCount(),
            ];
        }

        return $data;
    }

    public function supplier($suppliers)
    {
        $data = [];
        foreach ($suppliers as $supplier) {
            $data[] = [
                'id' => $supplier->id,
                'title' => $supplier->name,
                'url' => '/search?status=active&supplier=' . $supplier->id,
                'image' => $supplier->hasImage() ? $supplier->getImageUrl() : '/images/placeholder.jpg',
            ];
        }

        return $data;
    }

    public function posts($posts)
    {
        $userVotes = (new ForumRespository)
            ->getUserVotes(Auth::user()->id);

        $data = [];
        foreach ($posts as $post) {
            $data[] = [
                'id' => $post->id,
                'title' => $post->title,
                'body' => $post->body,
                'canEdit' => $post->user_id == Auth::user()->id,
                'tender' => [
                    'id' => $post->tender->id,
                    'title' => $post->tender->title,
                    'start' => $post->tender->started_at->toFormattedDateString(),
                    'end' => $post->tender->ended_at->toFormattedDateString(),
                ],
                'user' => [
                    'id' => $post->user_id,
                    'name' => $post->user->name,
                ],
                'supplier' => [
                    'name' => $post->tender->supplier->name,
                    'id' => $post->tender->supplier->id,
                ],
                'comments' => $post->comments()->count(),
                'votes' => [
                    'total' => $post->rates,
                    'isVoted' => $this->postVoted($userVotes, $post->id),
                ],
                'date' => $post->created_at->diffForHumans(),
            ];
        }
        return $data;
    }

    public function comments($comments)
    {
        $data = [];
        foreach ($comments as $comment) {
            $data[] = [
                'id' => $comment->id,
                'body' => $comment->body,
                'canEdit' => $comment->user_id == Auth::user()->id,
                'user' => [
                    'id' => $comment->user->id,
                    'name' => $comment->user->name,
                ],
                'date' => $comment->created_at->diffForHumans(),
            ];
        }
        return $data;
    }

    public function postVoted($list, $id)
    {
        return $list[$id] ?? 0;
    }

    public function canDownloadReport(Parcel $parcel)
    {
        $user = Auth::user();

        if ($parcel->user_id == $user->id) {
            return true;
        }

        if ($user->hasActiveSubscribe()) {
            return true;
        }

        if (Arr::has($user->buy(), $parcel->thread)) {
            return true;
        }

        if (Arr::has($user->tenderCanDownload(), $parcel->tender_id)) {
            return true;
        }

        return false;
    }

    public function canDownloadReports(Parcel $parcel)
    {
        $user = Auth::user();

        if ($user->hasActiveSubscribe()) {
            return true;
        }

        if (Arr::has($user->buy(), $parcel->thread)) {
            return true;
        }

        if (Arr::has($user->tenderCanDownload(), $parcel->tender_id)) {
            return true;
        }

        return false;

    }
}
