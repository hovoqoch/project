<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Settings
{
    public function get(string $type)
    {
        if (method_exists($this, $type)) {
            return call_user_func([$this, $type]);
        }
    }

    private function menuLinks()
    {
        Log::info(request()->path());
        return [
            [
                'text' => trans('Browse'),
                'url' => route('search'),
                'dropdown' => false,
                'class' => request()->is('search') ? 'active' : '',
            ],
            [
                'text' => trans('Tenders'),
                'url' => route('tender.index'),
                'dropdown' => false,
                'class' => request()->is('tenders') ? 'active' : '',
            ],
            [
                'text' => trans('Plans'),
                'url' => route('page', 'plans'),
                'dropdown' => false,
                'class' => request()->is('page/plans') ? 'active' : '',
            ],
            [
                'text' => trans('Chat'),
                'url' => route('show.forum'),
                'dropdown' => false,
                'class' => request()->is('chat') ? 'active' : '',
            ],
            [
                'text' => trans('Guide'),
                'dropdown' => true,
                'class' => request()->is('page/buyer') || request()->is('page/earning-money') ? 'active' : '',
                'list' => [
                    [
                        'url' => '/page/buyer',
                        'text' => trans('Buyer'),
                        'class' => request()->path() == 'page/buyer' ? 'active' : '',
                    ],
                    [
                        'url' => '/page/earning-money',
                        'text' => trans('Earning money'),
                        'class' => request()->path() == 'page/earning-money' ? 'active' : '',
                    ],
                ],
            ],
        ];
    }

    private function footerLinks()
    {
        return [
            ['text' => trans('Home'), 'url' => route('home'), 'active' => request()->is('/') ? 'active' : ''],
            ['text' => trans('Tenders'), 'url' => route('tender.index'), 'active' => request()->is('/tenders/*') ? 'active' : ''],
            ['text' => trans('Plans'), 'url' => route('page', 'plans'), 'active' => request()->is('/page/plans') ? 'active' : ''],
            ['text' => trans('Chat'), 'url' => route('show.forum'), 'active' => request()->is('/chat') ? 'active' : ''],
        ];
    }

    private function socialLinks()
    {
        return [
            ['icon' => "<i class='fab fa-facebook'></i>", 'url' => '#'],
            ['icon' => "<i class='fab fa-twitter'></i>", 'url' => '#'],
            ['icon' => "<i class='fab fa-linkedin-in'></i>", 'url' => '#'],
            ['icon' => "<i class='fab fa-instagram'></i>", 'url' => '#'],
        ];
    }

    private function countryList()
    {
        return [
            ['id' => 'EG', 'title' => 'EG'],
            ['id' => 'EG', 'title' => 'EG'],
        ];
    }

    private function statusList()
    {
        return [
            ['id' => 'closed', 'title' => 'Closed'],
            ['id' => 'active', 'title' => 'Active'],
            ['id' => 'upcoming', 'title' => 'Upcoming'],
        ];
    }

    private function colorList()
    {
        return [
            ['id' => 'D', 'title' => 'D'],
            ['id' => 'E', 'title' => 'E'],
            ['id' => 'F', 'title' => 'F'],
            ['id' => 'G', 'title' => 'G'],
            ['id' => 'H', 'title' => 'H'],
            ['id' => 'I', 'title' => 'I'],
            ['id' => 'J', 'title' => 'J'],
            ['id' => 'K', 'title' => 'K'],
            ['id' => 'L', 'title' => 'L'],
            ['id' => 'M', 'title' => 'M'],
            ['id' => 'Brown (light)', 'title' => 'Brown (light)'],
            ['id' => 'Brown (Medium)', 'title' => 'Brown (Medium)'],
            ['id' => 'Brown (Dark)', 'title' => 'Brown (Dark)'],
            ['id' => 'Fancy yellow (Light fancy)', 'title' => 'Fancy yellow (Light fancy)'],
            ['id' => 'Fancy yellow (Fancy)', 'title' => 'Fancy yellow (Fancy)'],
            ['id' => 'Fancy yellow (Intense)', 'title' => 'Fancy yellow (Intense)'],
            ['id' => 'Fancy yellow (Vivid)', 'title' => 'Fancy yellow (Vivid)'],
            ['id' => 'Blue (Light fancy)', 'title' => 'Blue (Light fancy)'],
            ['id' => 'Blue (Fancy)', 'title' => 'Blue (Fancy)'],
            ['id' => 'Blue (Intense)', 'title' => 'Blue (Intense)'],
            ['id' => 'Blue (Vivid)', 'title' => 'Blue (Vivid)'],
            ['id' => 'Pink (Light fancy)', 'title' => 'Pink (Light fancy)'],
            ['id' => 'Pink (Fancy)', 'title' => 'Pink (Fancy)'],
            ['id' => 'Pink (Intense)', 'title' => 'Pink (Intense)'],
            ['id' => 'Pink (Vivid)', 'title' => 'Pink (Vivid)'],
        ];
    }

    private function fluorScenceList()
    {
        return [
            ['id' => 'None', 'title' => 'None'],
            ['id' => 'Faint', 'title' => 'Faint'],
            ['id' => 'Medium', 'title' => 'Medium'],
            ['id' => 'Strong', 'title' => 'Strong'],
            ['id' => 'Very Strong', 'title' => 'Very Strong'],
        ];
    }

    private function plans()
    {
        $auth = Auth::check();
        $hasActiveSubscribe = false;
        if ($auth) {
            $hasActiveSubscribe = Auth::user()->hasActiveSubscribe();
        }
        return [
            'individual' => ['text' => 'Get Started',
                'url' => $auth ? '/search' : '',
                'auth' => $auth,
            ],
            'tender' => [
                'text' => 'Purchase',
                'login' => false,
                'auth' => $auth,
                'url' => $auth ? '/my/subscription' : '/tenders',
            ],
            'prime' => $this->prime(),
        ];
    }

    private function prime()
    {
        $user = Auth::user();

        if (!$user) {
            return [
                'status' => 0,
                'text' => 'Free Trial',
            ];
        }

        if (!$user->gotTrialSubscribe()) {
            return [
                'status' => 1,
                'text' => 'Free Trial',
            ];
        }

        if ($user->hasActiveSubscribe()) {
            return [
                'status' => 2,
                'text' => 'Browse',
                'url' => '/search',
            ];
        }

        return [
            'status' => 3,
            'text' => 'Purchase',
            'payment' => [
                'payload' => [
                    'points' => appPackages()['month']['points'],
                    'action' => 'buyMonth',
                ],
                'paypal' => [
                    'description' => "Subscribe for month",
                    'amount' => [
                        'currency_code' => 'USD',
                        'value' => appPackages()['month']['price'],
                    ],
                ],
            ],
        ];

    }
}
