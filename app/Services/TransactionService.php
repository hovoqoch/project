<?php

namespace App\Services;

use App\Events\TransactionAdded;
use App\Exceptions\NoPointsException;
use App\Models\Subscribe;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionService
{
    const MONTHLY = 0;
    const TRIAL = 1;

    public function addOrRemoveWishlist($userId, $thread, $inWishlist)
    {
        $transaction = Transaction::whereUserId($userId)
            ->whereThread($thread)
            ->first();

        if (!$transaction) {
            $transaction = new Transaction;
        }

        $transaction->thread = $thread;
        $transaction->user_id = $userId;
        $transaction->in_wishlist = $inWishlist;
        $transaction->save();

        return $transaction;
    }

    public function subtractFromBonus($amount, $for = '')
    {
        if (Auth::user()->points < $amount) {
            throw new NoPointsException('You don\'t have enough points ');
        }

        Auth::user()->deactivateBonus($amount, [
            'type' => 'POINTS',
            'action' => [
                'for' => $for,
                'effect' => 'SUBTRACT',
            ],
        ]);

        return true;
    }

    public function buyTender(Request $request)
    {
        $package = appPackages()['tender'];
        $userId = $request->user()->id;
        $method = $request->method;
        $tender = $request->tender;

        if ($method == 'bonus') {
            $results = $this->subtractFromBonus($package['points'], 'SUBSCRIBE TENDER #' . $request->tender);

            if (!$results) {
                return false;
            }
        }

        $transaction = new Transaction;
        $transaction->tender_id = $tender;
        $transaction->user_id = $userId;
        $transaction->in_wishlist = false;
        $transaction->done = true;
        $transaction->save();

        event(new TransactionAdded('SUBSCRIBE TENDER #' . $request->tender));

        return $transaction;
    }

    public function buyThread(Request $request)
    {
        $package = appPackages()['thread'];
        $userId = $request->user()->id;
        $method = $request->method;
        $thread = $request->thread;

        if ($method == 'bonus') {
            $results = $this->subtractFromBonus($package['points'], 'SUBSCRIBE PARCEL #' . $request->thread);

            if (!$results) {
                return false;
            }
        }

        $transaction = new Transaction;
        $transaction->thread = $thread;
        $transaction->user_id = $userId;
        $transaction->in_wishlist = false;
        $transaction->done = true;
        $transaction->save();

        event(new TransactionAdded('SUBSCRIBE PARCEL #' . $request->thread));

        return $transaction;
    }

    public function buyMonth(Request $request)
    {
        $package = appPackages()['month'];
        $userId = $request->user()->id;
        $method = $request->method;

        if ($method == 'bonus') {
            $results = $this->subtractFromBonus($package['points'], 'SUBSCRIBE MONTH');

            if (!$results) {
                return false;
            }
        }

        $subscribe = new Subscribe;
        $subscribe->user_id = $userId;
        $subscribe->ended_at = Carbon::now()->addMonths(1);
        $subscribe->type = self::MONTHLY;
        $subscribe->save();

        event(new TransactionAdded('SUBSCRIBE MONTH'));

        return $subscribe;
    }

    public function trial(Request $request)
    {
        $userId = $request->user()->id;

        $exists = Subscribe::whereUserId($userId)->whereType(self::TRIAL)->first();

        if ($exists) {
            return false;
        }

        $subscribe = new Subscribe;
        $subscribe->user_id = $userId;
        $subscribe->ended_at = Carbon::now()->addDays(14);
        $subscribe->type = self::TRIAL;
        $subscribe->save();

        return $subscribe;
    }

    public function buyPoints(Request $request)
    {
        Auth::user()->addBonus($request->points, [
            'type' => 'POINTS',
            'action' => [
                'for' => 'SUBSCRIBE #' . $request->points,
                'effect' => 'ADD',
            ],
        ]);

        event(new TransactionAdded('SUBSCRIBE #' . $request->points));

        return true;
    }
}
