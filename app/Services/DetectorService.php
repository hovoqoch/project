<?php
/**
 * This file is part of safetrade app.
 *
 * @author Omar Makled <omar.makled@gmail.com>
 */
namespace App\Services;

use DeviceDetector\DeviceDetector;
use Illuminate\Support\Arr;

class DetectorService
{
    /**
     * Detect agent
     *
     * @param string $agent
     * @return array
     */
    public static function detect(string $agent): array
    {
        $dd = new DeviceDetector($agent);
        $dd->parse();
        if ($dd->isBot()) {
            return ['bot' => $dd->getBot()];
        }
        return [
            'clientInfo' => $dd->getClient(),
            'osInfo' => $dd->getOs(),
            'device' => $dd->getDeviceName(),
            'brand' => $dd->getBrandName(),
            'model' => $dd->getModel(),
        ];
    }

    /**
     * Parse info to string
     *
     * @param array $info
     * @return string
     */
    public static function parse(array $info)
    {
        return implode(Arr::dot($info), ' ');
    }
}
