<?php

namespace App\Services;

use Carbon\Carbon;

class Dt
{
    /**
     * today = 15, $start = 16 (16 - 15 = 1) > 0 true
     * today = 15, $start = 14 (14 - 15 = -1) > 0 false
     *
     * today = 15, $end = 14 (14 - 15 = -1) < 0 true
     * today = 15, $end = 16 (16 - 15 = 1) < 0 false
     *
     * @param \Carbon\Carbon $startAt
     * @param \Carbon\Carbon $endAt
     * @return void
     */
    public static function status(Carbon $startAt, Carbon $endAt)
    {
        if (Carbon::now()->diffInDays($startAt, false) > 0) {
            return 'upcommnig';
        }

        if (Carbon::now()->diffInDays($endAt, false) < 0) {
            return 'closed';
        }

        return 'active';
    }

    public static function isActive(Carbon $startAt, Carbon $endAt)
    {
        return self::status($startAt, $endAt) == 'active';
    }
}
