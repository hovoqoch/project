<?php

namespace App\Services;

use Illuminate\Support\Facades\Cookie;

class CookieService
{
    public function append($key, $ids)
    {
        if (!$cookies = Cookie::get($key)) {
            $cookies = [];
        } else {
            $cookies = json_decode($cookies, true);
        }

        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as $id) {
            $cookies[$id] = $id;
        }

        Cookie::queue($key, json_encode($cookies));
    }

    public function get($key)
    {
        if (!$cookies = Cookie::get($key)) {
            $cookies = [];
        } else {
            $cookies = json_decode($cookies, true);
        }

        return $cookies;
    }
}
