<?php

namespace App\Notifications;

use App\Models\Parcel;
use Illuminate\Notifications\Notification;

class PointSubtractedNotification extends Notification
{
/**
 *
 * @var \App\Models\Parcel
 */
    private $report;

    /**
     *
     * @param \App\Models\Parcel $report
     */
    public function __construct(Parcel $report)
    {
        $this->report = $report;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            sprintf("Points was subtracted click <a href='%s'>here</a>",
                route('my.bonus')
            ),
        ];
    }
}
