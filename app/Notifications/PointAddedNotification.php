<?php

namespace App\Notifications;

use App\Models\Parcel;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PointAddedNotification extends Notification
{
    /**
     *
     * @var \App\Models\Parcel
     */
    private $report;

    /**
     *
     * @param \App\Models\Parcel $report
     */
    public function __construct(Parcel $report)
    {
        $this->report = $report;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Hello {$notifiable->name}")
            ->line('You got new points for uploading new report')
            ->action('View Report', route('parcel.show', [$this->report->id, $this->report->thread]))
            ->line('Thank you for using our application!');
    }
}
