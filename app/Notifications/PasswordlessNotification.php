<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class PasswordlessNotification extends Notification
{

    private $token;

    /**
     *
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toMail()
    {
        return (new MailMessage)
            ->subject('Verify your email address')
            ->greeting("Hello!")
            ->line(new HtmlString("Thanks for your registration, Use this code <strong>{$this->token}</strong> to verify your email address"))
            ->line('Thank you for using our application!');
    }
}
