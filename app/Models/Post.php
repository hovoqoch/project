<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\Rate;
use App\Models\Tender;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function tender()
    {
        return $this->belongsTo(Tender::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function votes()
    {
        return $this->morphMany(Rate::class, 'subjectable');
    }

    public function scopeTenders($query, ?array $ids)
    {
        if (!$ids) {
            return $query;
        }

        return $query->whereIn('tender_id', $ids);
    }
}
