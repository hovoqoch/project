<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'ended_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function remaining()
    {
        return Carbon::now()->diffInDays($this->ended_at, false);
    }

    public function presentType()
    {
        if ($this->type == 0) {
            return 'Montly';
        }

        if ($this->type == 1) {
            return 'Trial';
        }
    }
}
