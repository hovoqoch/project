<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class LoginToken extends Model
{
    /**
     * Fillable fields for the model.
     *
     * @var array
     */
    protected $fillable = ['email', 'token'];

    public $timestamps = false;

    const UPDATED_AT = null;

    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
