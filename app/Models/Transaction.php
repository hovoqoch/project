<?php

namespace App\Models;

use App\Models\Parcel;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tender()
    {
        return $this->belongsTo(Tender::class);
    }

    public function parcel()
    {
        return $this->belongsTo(Parcel::class, 'thread');
    }
}
