<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AuthProvider extends Model
{
    protected $fillable = ['user_id', 'provider', 'identifier'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user associated with the provider so that they can be logged in.
     *
     * @param string $provider
     * @param string $identifier
     * @return User|null
     */
    public static function logIn(string $provider, string $identifier): ?User
    {
        if ($provider = static::where(compact('provider', 'identifier'))->first()) {
            $provider->touch();

            return $provider->user;
        }

        return null;
    }
}
