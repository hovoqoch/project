<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

trait MediaTrait
{
    use HasMediaTrait;

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(240);

        $this->addMediaConversion('blur')
            ->width(200)->blur(90);
    }

    public function hasImage()
    {
        return (bool) $this->getMedia('images')->first();
    }

    public function hasFile()
    {
        return (bool) $this->getMedia('files')->first();
    }

    public function getImages()
    {
        return $this->getMedia('images');
    }

    public function getImage($placeholder = false)
    {
        if ($image = $this->getMedia('images')->first()) {
            return $image->getUrl();
        }

        if ($placeholder) {
            return '/images/placeholder.jpg';
        }
    }

    public function getImageUrl()
    {
        if ($image = $this->getMedia('images')->first()) {
            return $image->getUrl('thumb');
        }
    }

    public function getMainImageUrl()
    {
        if ($image = $this->getMedia('images')->first()) {
            return $image->getUrl();
        }
    }

    public function getBlurUrl()
    {
        if ($image = $this->getMedia('images')->first()) {
            return $image->getUrl('blur');
        }
    }

    public function getAvatar()
    {
        if ($image = $this->getMedia('images')->first()) {
            return $image->getUrl('thumb');
        }

        return '/images/avatar.png';
    }

    public function getSlider()
    {
        $images = [];
        foreach ($this->getMedia('images') as $image) {
            $images[] = [
                'url' => $image->getUrl('thumb'),
            ];
        }
        return [
            'images' => $images,
            'count' => count($images),
            'hasSlider' => count($images) > 0,
        ];
    }
}
