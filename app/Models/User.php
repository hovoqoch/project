<?php

namespace App\Models;

use App\Models\AuthProvider;
use App\Models\Bonus;
use App\Models\Log;
use App\Models\MediaTrait;
use App\Models\Rate;
use App\Models\Subscribe;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, MediaTrait, HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'device', 'is_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'device' => 'array',
    ];

    /**
     * Get the user's login providers.
     */
    public function authProvider()
    {
        return $this->hasOne(AuthProvider::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function subscribes()
    {
        return $this->hasMany(Subscribe::class);
    }

    public function votes()
    {
        return $this->hasMany(Rate::class);
    }

    public function wishlist()
    {
        return $this->transactions()
            ->where('in_wishlist', true)
            ->get()
            ->pluck('thread', 'thread')
            ->toArray();
    }

    public function cart()
    {
        return $this->transactions()
            ->where('in_cart', true)
            ->get()
            ->pluck('parcel_id', 'parcel_id')
            ->toArray();
    }

    public function buy()
    {
        return $this->transactions()
            ->where('done', true)
            ->get()
            ->pluck('thread', 'thread')
            ->toArray();
    }

    public function isVotedFor($type, $parcel)
    {
        $data = $this->votes()
            ->select('value')
            ->whereType($type)
            ->whereSubjectableId($parcel)
            ->whereSubjectableType(Parcel::class)
            ->first();

        if ($data) {
            return $data->value;
        }

        return 0;
    }

    public function isVotedUp($type, $parcel)
    {
        $data = $this->votes()
            ->select('id')
            ->whereType($type)
            ->whereParcelId($parcel)
            ->whereValue(true)
            ->first();

        return $data ? 'true' : 'false';
    }

    public function isVotedDown($type, $parcel)
    {
        $data = $this->votes()
            ->select('id')
            ->whereType($type)
            ->whereParcelId($parcel)
            ->whereValue(false)
            ->first();

        return $data ? 'true' : 'false';
    }

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function activeBonus()
    {
        return $this->hasMany(Bonus::class)
            ->count()
            ->first()
            ->count;
    }

    public function hasActiveSubscribe()
    {
        $latest = $this->subscribes()
            ->select('ended_at')
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();

        if (!$latest) {
            return false;
        }

        if (Carbon::now()->diffInDays($latest->ended_at, false) < 0) {
            return false;
        }

        return true;
    }

    public function gotTrialSubscribe()
    {
        $latest = $this->subscribes()
            ->selectRaw('type')
            ->first();

        return (bool) $latest;
    }

    public function deactivateBonus($amount, $log)
    {
        $this->points -= $amount;
        $this->save();

        $action = [
            'points' => $amount,
            'for' => $log['action']['for'],
            'effect' => $log['action']['effect'],
        ];

        Log::create([
            'user_id' => $this->id,
            'type' => $log['type'],
            'action' => $action,
        ]);
    }

    public function addBonus($amount, $log)
    {
        $this->points += $amount;
        $this->save();
        $action = [
            'points' => $amount,
            'for' => $log['action']['for'],
            'effect' => $log['action']['effect'],
        ];
        Log::create([
            'user_id' => $this->id,
            'type' => $log['type'],
            'action' => $action,
        ]);
    }

    public function tenderCanDownload()
    {
        return $this->transactions()
            ->whereNotNull('tender_id')
            ->get()
            ->pluck('tender_id', 'tender_id');
    }

    public function voteComments()
    {
        return $this->hasMany(Rate::class)
            ->whereSubjectableType(Post::class);
    }
}
