<?php

namespace App\Models;

use App\Models\Parcel;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function parcel()
    {
        return $this->belongsTo(Parcel::class);
    }

    public function scopeCount($query)
    {
        return $query
            ->selectRaw('sum(amount) as count')
            ->whereIsActive(true);
    }
}
