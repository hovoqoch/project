<?php

namespace App\Models;

use App\Models\MediaTrait;
use App\Models\Rate;
use App\Models\Review;
use App\Models\Tender;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;

class Parcel extends Model implements HasMedia
{
    use MediaTrait;

    private $reportIds = [];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tender()
    {
        return $this->belongsTo(Tender::class);
    }

    public function reports()
    {
        return $this->hasMany(Parcel::class, 'parcel_number', 'parcel_number')
            ->where('id', '!=', $this->id);
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'subjectable');
    }

    public function rates()
    {
        return $this->morphMany(Review::class, 'subjectable')
            ->selectRaw('count(reviews.id) as count, reviews.rate as rate')
            ->groupBy('reviews.rate');
    }

    public function getVotes()
    {
        return $this->morphMany(Rate::class, 'subjectable');
    }

    public function votes($type, $value)
    {
        return $this->morphMany(Rate::class, 'subjectable')
            ->selectRaw('count(id) as count')
            ->whereValue($value)
            ->whereType($type)
            ->first()
            ->count;
    }

    public function avgRates()
    {
        return $this->morphMany(Review::class, 'subjectable')
            ->selectRaw('avg(rate) as avg, count(*) as count');
    }

    public function avgRate()
    {
        // todo
        $rate = $this->avgRates()->first();
        if ($rate) {
            return [
                '_avg' => number_format($rate->avg, 2),
                'avg' => round($rate->avg),
                'count' => $rate->count,
            ];
        }

        return ['_avg' => 0, 'avg' => 0, 'count' => 0];
    }

    public function scopeTender($query, ?string $tender)
    {
        if ($tender) {
            $query->whereTenderId($tender);
        }

        return $query;
    }

    public function scopeColorByMachine($query, ?string $color)
    {
        if ($color) {
            $query->whereColorByMachine($color);
        }

        return $query;
    }

    public function scopeColorByEye($query, ?string $color)
    {
        if ($color) {
            $query->whereColorByEye($color);
        }

        return $query;
    }

    public function scopeFlourscene($query, ?string $flourscene)
    {
        if ($flourscene) {
            $query->whereFlourscene($flourscene);
        }

        return $query;
    }

    public function scopeLive($query)
    {
        return $query->whereIsDraft(false);
    }

    public function scopeExclude($query, $id = null)
    {
        if (!$id) {
            return $query;
        }

        return $query->where('id', '!=', $id);
    }

    public function scopeDraft($query)
    {
        return $query->whereIsDraft(true);
    }

    public function scopeValuation($query, ?array $data)
    {
        if (
            isset($data['valuation_from']) &&
            isset($data['valuation_to'])) {

            $from = $data['valuation_from'];
            $to = $data['valuation_to'];

            $query->whereRaw("`valuation_from` >= $from and `valuation_to` <= $to");
        }

        return $query;
    }

    public function scopeSize($query, ?array $data)
    {
        if (
            isset($data['size_from']) &&
            isset($data['size_to'])) {

            $from = $data['size_from'];
            $to = $data['size_to'];

            $query->whereRaw("`size` >= $from and `size` <= $to");
        }

        return $query;
    }

    public function countAll($tenderId, $parcelNumber)
    {
        return DB::table('parcels')
            ->selectRaw('count(id) as aggregation')
            ->whereTenderId($tenderId)
            ->whereParcelNumber($parcelNumber)
            ->groupBy('parcel_number')
            ->first()
            ->aggregation;
    }

    public function presentComment($field)
    {
        if (!$this->$field) {
            return '&nbsp;';
        }

        return $this->$field;
    }

    public function scopeMostDownload($query, $limit = 8)
    {
        $thread = Parcel::selectRaw("thread, sum(download) as download")
            ->groupBy('thread')
            ->orderBy('download', 'desc')
            ->limit(8)
            ->get()
            ->pluck('thread', 'thread');
        return $thread;
        // $thread = DB::select("select thread, sum(download) as download from parcels group by thread order by download desc limit 5")
        // $ids = DB::select("select parcels.id from (
        // select max(download) as download, thread from parcels group by thread
        // ) as t
        // inner join parcels on parcels.thread = t.thread and parcels.download = t.download ");

        // $result = array_map(function ($value) {
        //     return (array) $value;
        // }, $ids);

        // return $result;
    }

    public function threadRates()
    {
        $ids = Parcel::where('thread', $this->thread)
            ->select('id')
            ->get()
            ->pluck('id', 'id');

        return Review::whereIn('subjectable_id', $ids)
            ->selectRaw('avg(rate) as avg, count(*) as count')
            ->first()
            ->toArray();
    }

    public function reportIds()
    {
        if (!$this->reportIds) {
            $this->reportIds = Parcel::whereThread($this->thread)
                ->live()
                ->get()
                ->pluck('id', 'id');
        }
        return $this->reportIds;

    }

    public function threadImages()
    {
        return Media::with(['model'])->whereIn('model_id', $this->reportIds())
            ->where('model_type', 'App\\Models\\Parcel')
            ->where('collection_name', 'images')
            ->get();
    }

    public function threadReviews()
    {
        return Review::with(['user'])->whereIn('subjectable_id', $this->reportIds())
            ->where('subjectable_type', 'App\\Models\\Parcel')
            ->get();
    }

    public function getThread()
    {
        return Parcel::with(['media'])
            ->whereThread($this->thread)
            ->live()
            ->get();
    }

    public function scopeIdsByThread($query, string $thread)
    {
        return $query->whereThread($thread)
            ->live()
            ->get()
            ->pluck('id', 'id');
    }

    public function images()
    {
        return $this->morphMany(config('medialibrary.media_model'), 'model')
            ->where('collection_name', 'images');
    }

    public function files()
    {
        return $this->morphMany(config('medialibrary.media_model'), 'model')
            ->where('collection_name', 'files');
    }
}
