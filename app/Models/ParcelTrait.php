<?php
namespace App\Models;

trait ParcelTrait
{

    public function reviews2()
    {
        return $this->morphMany(Review::class, 'subjectable');
    }

    public function rates2()
    {
        return $this->morphMany(Review::class, 'subjectable');
    }
}
