<?php

namespace App\Models;

use App\Models\MediaTrait;
use App\Models\Parcel;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Tender extends Model implements HasMedia
{
    use MediaTrait;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'started_at', 'ended_at', 'bid_started_at', 'bid_ended_at'];

    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }

    public function parcelsCount()
    {
        return $this->parcels()
            ->selectRaw('count(distinct(parcel_number)) as count')
            ->live()
            ->first()
            ->count;
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function formatStatus()
    {
        return \App\Services\Dt::status(
            $this->started_at,
            $this->ended_at
        );
    }

    public function formatDate($field)
    {
        if ($this->$field) {
            return $this->$field->format('d/m/Y');
        }
    }

    public function scopeSupplier($query, ?int $supplier)
    {
        if ($supplier) {
            return $query->whereSupplierId($supplier);
        }
    }

    public function scopeStatus($query, ?string $status)
    {
        if ($status) {
            $fn = ucfirst($status);
            return $query->$fn();
        }
    }

    public function scopeActive($query)
    {
        $today = date('Y-m-d');

        return $query->whereRaw("date(tenders.ended_at) >= '$today' and date(tenders.started_at) <= '$today'");
    }

    public function scopeUpcoming($query)
    {
        $today = date('Y-m-d');

        return $query->whereRaw("date(tenders.started_at) > '$today'");
    }

    public function scopeClosed($query)
    {
        $today = date('Y-m-d');

        return $query->whereRaw("date(tenders.ended_at) < '$today'");
    }

    public function isActive()
    {
        return \App\Services\Dt::isActive(
            $this->started_at,
            $this->ended_at
        );
    }

    public function scopeCountry($query, ?string $country)
    {
        if ($country) {
            $query->whereCountry($country);
        }

        return $query;
    }
}
