<?php

namespace App\Models;

use App\Models\MediaTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Supplier extends Model implements HasMedia
{
    use MediaTrait;

    protected $guarded = [];
}
