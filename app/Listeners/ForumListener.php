<?php

namespace App\Listeners;

use GuzzleHttp\Client;

class ForumListener
{
    /**
     * Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * New CloudService Instance.
     *
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('FBI_DB', 'https://forum-b9548.firebaseio.com/'),
        ]);
    }

    /**
     *
     * @param object $event
     * @return void
     */
    public function addTender($event)
    {
        $response = $this->client->post('category.json', [
            'json' => $event->toArray(),
        ]);

        $ref = json_decode($response->getBody(), true)['name'];
        $event->ref = $ref;
        $event->save();
    }

    /**
     *
     * @param object $event
     * @return void
     */
    public function updateTender($event)
    {
        $this->client->post("category/${$event->ref}.json", [
            'json' => $event->toArray(),
        ]);
    }

    /**
     * Handle the event.
     *
     * @param  object  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'tender.was.added',
            'App\Listeners\ForumListener@addTender'
        );

        $events->listen(
            'tender.was.updated',
            'App\Listeners\ForumListener@updateTender'
        );
    }
}
