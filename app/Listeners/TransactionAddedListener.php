<?php

namespace App\Listeners;

use App\Events\TransactionAdded;
use App\Notifications\TransactionAddedNotification;
use Illuminate\Support\Facades\Notification;

class TransactionAddedListener
{
    /**
     * Handle the event.
     *
     * @param  TransactionAdded  $event
     * @return void
     */
    public function handle(TransactionAdded $event)
    {
        Notification::send($event->user,
            new TransactionAddedNotification($event->type)
        );
    }
}
