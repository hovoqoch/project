<?php

namespace App\Listeners;

use App\Events\ReportRated;
use App\Notifications\ReportRatedNotification;
use App\Repositories\UserRespository;
use Illuminate\Support\Facades\Notification;

class ReportRatedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportRated  $event
     * @return void
     */
    public function handle(ReportRated $event)
    {
        $repo = new UserRespository;
        $notification = new ReportRatedNotification($event->parcel);

        Notification::send($event->parcel->user, $notification);
        Notification::send($repo->getMonthlySubscribers(), $notification);
        Notification::send($repo->getTenderSubscribers($event->parcel->tender), $notification);
        Notification::send($repo->getParcelSubscribers($event->parcel), $notification);
    }
}
