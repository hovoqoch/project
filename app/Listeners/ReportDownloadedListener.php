<?php

namespace App\Listeners;

use App\Models\Parcel;
use Illuminate\Support\Facades\DB;

class ReportDownloadedListener
{
    const REPORT_DOWNLOAD = 'report.download';

    const REPORTS_DOWNLOAD = 'reports.download';

    const COOKIE = 'downloaded_reports';

    /**
     *
     * @param object $event
     * @return void
     */
    public function reportDownload($event)
    {
        $event->download += 1;
        $event->save();
    }

    /**
     *
     * @param object $ids
     * @return void
     */
    public function reportsDownload($ids)
    {
        Parcel::whereIn('id', $ids)
            ->update([
                "download" => DB::raw("`download` + 1"),
            ]);
    }

    /**
     * Handle the event.
     *
     * @param  object  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            self::REPORT_DOWNLOAD,
            'App\Listeners\ReportDownloadedListener@reportDownload'
        );

        $events->listen(
            self::REPORTS_DOWNLOAD,
            'App\Listeners\ReportDownloadedListener@reportsDownload'
        );
    }
}
