<?php

namespace App\Listeners;

use App\Events\ReportAdded;
use App\Notifications\PointAddedNotification;
use Illuminate\Support\Facades\Notification;

class PointAddedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportAdded  $event
     * @return void
     */
    public function handle(ReportAdded $event)
    {
        Notification::send($event->parcel->user, new PointAddedNotification($event->parcel));
    }
}
