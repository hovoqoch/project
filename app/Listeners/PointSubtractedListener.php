<?php

namespace App\Listeners;

use App\Events\ReportDeleted;
use App\Notifications\PointSubtractedNotification;
use Illuminate\Support\Facades\Notification;

class PointSubtractedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportDeleted  $event
     * @return void
     */
    public function handle(ReportDeleted $event)
    {
        Notification::send($event->parcel->user, new PointSubtractedNotification($event->parcel));
    }
}
