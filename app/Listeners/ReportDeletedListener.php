<?php

namespace App\Listeners;

use App\Events\ReportDeleted;
use App\Notifications\ReportDeletedNotification;
use App\Repositories\UserRespository;
use Illuminate\Support\Facades\Notification;

class ReportDeletedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportDeleted  $event
     * @return void
     */
    public function handle(ReportDeleted $event)
    {
        $repo = new UserRespository;
        $notification = new ReportDeletedNotification($event->parcel);

        Notification::send($repo->getMonthlySubscribers(), $notification);
        Notification::send($repo->getTenderSubscribers($event->parcel->tender), $notification);
        Notification::send($repo->getParcelSubscribers($event->parcel), $notification);
    }
}
