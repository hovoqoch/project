<?php

namespace App\Listeners;

use App\Events\ReportViewed;
use App\Notifications\ReportViewedNotification;
use App\Repositories\UserRespository;
use Illuminate\Support\Facades\Notification;

class ReportViewedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportViewed  $event
     * @return void
     */
    public function handle(ReportViewed $event)
    {
        $repo = new UserRespository;

        $notification = new ReportViewedNotification($event->parcel);
        Notification::send($repo->getThread($event->parcel), $notification);
    }
}
