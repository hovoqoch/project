<?php

namespace App\Listeners;

use App\Events\ReportAdded;
use App\Notifications\ReportAddedNotification;
use App\Repositories\UserRespository;
use Illuminate\Support\Facades\Notification;

class ReportAddedListener
{
    /**
     * Handle the event.
     *
     * @param  ReportAdded  $event
     * @return void
     */
    public function handle(ReportAdded $event)
    {
        $repo = new UserRespository;
        $notification = new ReportAddedNotification($event->parcel);

        Notification::send($repo->getMonthlySubscribers(), $notification);
        Notification::send($repo->getTenderSubscribers($event->parcel->tender), $notification);
        Notification::send($repo->getParcelSubscribers($event->parcel), $notification);
    }
}
