<?php

namespace App\Observers;

use App\Models\Tender;

class TenderObserver
{
    /**
     *
     * @param  \App\Models\Tender  $tender
     * @return void
     */
    public function created(Tender $tender)
    {
        event('tender.was.added', $tender);
    }

    /**
     *
     * @param  \App\Models\Tender  $tender
     * @return void
     */
    public function updated(Tender $tender)
    {
        event('tender.was.updated', $tender);
    }
}
