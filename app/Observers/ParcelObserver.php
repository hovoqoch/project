<?php

namespace App\Observers;

use App\Models\Parcel;

class ParcelObserver
{
    /**
     * Handle the parcel "created" event.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return void
     */
    public function created(Parcel $parcel)
    {
        $parcel->thread = $parcel->parcel_number . '-' . $parcel->tender_id;
        $parcel->save();
    }

    /**
     * Handle the parcel "updated" event.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return void
     */
    public function updated(Parcel $parcel)
    {
        //
    }

    /**
     * Handle the parcel "deleted" event.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return void
     */
    public function deleted(Parcel $parcel)
    {
        //
    }

    /**
     * Handle the parcel "restored" event.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return void
     */
    public function restored(Parcel $parcel)
    {
        //
    }

    /**
     * Handle the parcel "force deleted" event.
     *
     * @param  \App\Models\Parcel  $parcel
     * @return void
     */
    public function forceDeleted(Parcel $parcel)
    {
        //
    }
}
