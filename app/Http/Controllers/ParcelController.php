<?php

namespace App\Http\Controllers;

use App\Listeners\ReportDownloadedListener;
use App\Models\Parcel;
use App\Models\Review;
use App\Models\Tender;
use App\Models\User;
use App\Repository;
use App\Serializer;
use App\Services\CookieService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Spatie\MediaLibrary\MediaStream;
use Spatie\MediaLibrary\Models\Media;

class ParcelController extends Controller
{
    private $rules = [
        'tender_id' => ['sometimes', 'required'],
        'parcel_number' => ['sometimes', 'required'],
        'size' => ['required', 'numeric'],
        'color_by_eye' => ['required', 'string'],
        'color_by_machine' => ['required', 'string'],
        'flourscene' => ['required', 'string'],
        'valuation_from' => ['required', 'numeric'],
        'valuation_to' => ['required', 'numeric'],
    ];

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $type = $request->get('type', 'live');

        if ($request->ajax()) {
            $parcels = (new Serializer)->parcel(
                Parcel::$type()->whereUserId(Auth::user()->id)->paginate(10)
            );

            return response()->json([
                'parcels' => $parcels,
            ]);
        }

        return view('parcel.index', compact('parcels', 'type'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $tenders = Tender::active()->select('title', 'id')->get()->toArray();
        $parcel = new Parcel;

        return view('parcel.create', compact('tenders', 'parcel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Parcel $parcel)
    {
        $tenders = Tender::active()->select('title', 'id')->get()->toArray();

        return view('parcel.edit', compact('tenders', 'parcel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Parcel $parcel, Request $request)
    {
        $this->addOrUpdate($parcel, $request);

        return redirect(route('parcel.edit', $parcel->id))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->only(
                'parcel_number',
                'tender_id',
                'size',
                'color_by_eye',
                'color_by_machine',
                'valuation_from',
                'flourscene',
                'valuation_to'
            ), $this->rules
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $parcel = (!$request->parcelId)
        ? new Parcel
        : Parcel::findOrFail($request->parcelId);

        $parcel->parcel_number = $request->parcel_number;
        $parcel->tender_id = $request->tender_id;
        $parcel->user_id = Auth::user()->id;
        $parcel->size = $request->size;
        $parcel->color_by_eye = $request->color_by_eye;
        $parcel->comment_color_by_eye = $request->comment_color_by_eye;
        $parcel->flourscene = $request->flourscene;
        $parcel->comment_flourscene = $request->comment_flourscene;
        $parcel->color_by_machine = $request->color_by_machine;
        $parcel->valuation_from = $request->valuation_from;
        $parcel->valuation_to = $request->valuation_to;
        $parcel->save();

        return response()->json([
            'uploadUrl' => route('parcel.media.upload', $parcel->id),
            'parcelId' => $parcel->id,
            'redirectTo' => route('parcel.edit', $parcel->id),
        ]);

        // $parcel = new Parcel;
        // $valid = $this->check($request);
        // if (!$valid) {
        //     return back()
        //         ->withInput()
        //         ->withStatus(
        //             json_encode(['type' => 'info', 'text' => 'Can not add more reports'])
        //         );
        // }
        // $parcel = $this->addOrUpdate($parcel, $request);

        // return redirect(route('parcel.edit', $parcel->id))
        //     ->withStatus(
        //         json_encode(['type' => 'success', 'text' => 'Success done'])
        //     );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Parcel $parcel, string $thread)
    {
        $cookieService = new CookieService;
        $cookieService->append('viewed_thread', $parcel->id);
        $downloaded = $cookieService->get(ReportDownloadedListener::COOKIE);

        $meta = (new Serializer)->meta($thread, $parcel);
        return view('parcel.show')->with([
            'meta' => $meta,
            'parcelId' => $parcel->id,
            'thread' => $thread,
            'reports' => Parcel::with(['tender'])->whereIn('id', $meta['reportIds'])->get(),
            'reviews' => Review::with(['user'])->forReports($meta['reportIds'])->orderBy('id', 'desc')->get(),
            'images' => Media::with(['model'])->whereIn('model_id', $meta['reportIds'])
                ->where('model_type', 'App\\Models\\Parcel')
                ->where('collection_name', 'images')
                ->get(),
            'rates' => (new Repository)->parcelStarts($meta['reportIds']),
            'serializer' => new Serializer,
            'downloaded' => $downloaded,
        ]);
    }

    private function addOrUpdate(Parcel $parcel, Request $request)
    {
        $request->validate($this->rules);
        $parcel->size = $request->size;
        $parcel->color_by_eye = $request->color_by_eye;
        $parcel->comment_color_by_eye = $request->comment_color_by_eye;
        $parcel->flourscene = $request->flourscene;
        $parcel->comment_flourscene = $request->comment_flourscene;
        $parcel->color_by_machine = $request->color_by_machine;
        $parcel->valuation_from = $request->valuation_from;
        $parcel->valuation_to = $request->valuation_to;
        $parcel->save();

        return $parcel;
    }

    public function media(Parcel $parcel, Request $request)
    {
        return view('parcel.media', compact('parcel'));
    }

    public function delete(Parcel $parcel)
    {
        $parcel->delete();
        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    public function canAdd(Request $request)
    {
        $validator = Validator::make(
            $request->only('parcel_number', 'tender_id'), [
                'parcel_number' => ['required'],
                'tender_id' => ['required'],
            ]
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $count = Parcel::selectRaw('count(id) as count')
            ->exclude($request->parcelId)
            ->whereParcelNumber($request->parcel_number)
            ->whereTenderId($request->tender_id)
            ->first()
            ->count;
        $found = Parcel::selectRaw('id')
            ->exclude($request->parcelId)
            ->whereParcelNumber($request->parcel_number)
            ->whereTenderId($request->tender_id)
            ->whereUserId(Auth::user()->id)
            ->first();

        return response()->json([
            'reports' => $count,
            'canAdd' => $count < 5 && !$found,
        ]);
    }

    public function download(Parcel $parcel)
    {
        $serializer = new Serializer;
        if ($serializer->canDownloadReport($parcel)) {
            $downloads = $parcel->getMedia('files');
            $file = 'report-' . $parcel->id . '-' . date('Y-m-d') . '.zip';
            event(ReportDownloadedListener::REPORT_DOWNLOAD, $parcel);
            (new CookieService)->append(ReportDownloadedListener::COOKIE, $parcel->id);
            return MediaStream::create($file)->addMedia($downloads);
        }

        return back()->withStatus(
            json_encode(['type' => 'info', 'text' => 'You can not download report/parcel'])
        );
    }

    public function downloadAll(Parcel $parcel)
    {
        $serializer = new Serializer;
        if ($serializer->canDownloadReports($parcel)) {
            $ids = Parcel::select('id')->where('thread', $parcel->thread)
                ->get()
                ->pluck('id', 'id');
            $downloads = Media::whereIn('model_id', $ids)
                ->where('model_type', 'App\\Models\\Parcel')
                ->where('collection_name', 'files')
                ->get();
            $file = 'report-all-' . $parcel->thread . '-' . date('Y-m-d') . '.zip';
            event(ReportDownloadedListener::REPORTS_DOWNLOAD, $ids);
            (new CookieService)->append(ReportDownloadedListener::COOKIE, $ids->toArray());
            return MediaStream::create($file)->addMedia($downloads);
        }

        return back()->withStatus(
            json_encode(['type' => 'info', 'text' => 'You can not download report/parcel'])
        );
    }

    public function myViewed(Request $request)
    {
        $cookies = (new CookieService)->get('viewed_thread');

        if ($request->ajax()) {
            $serializer = new Serializer;
            $parcels = $serializer->parcel(Parcel::live()->whereIn('id', $cookies)->paginate(10));

            return response()->json([
                'parcels' => $parcels,
            ]);
        }

        return view('my/viewed');
    }
}
