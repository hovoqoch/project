<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Repositories\ForumRespository;
use App\Serializer;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $posts = (new ForumRespository)->getBySupplier($request->get('supplier'));
            return response()->json([
                'url' => route('show.forum', $request->all()),
                'posts' => (new Serializer)->posts($posts),
            ]);
        }

        return view('forum.index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('forum.create');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showComments(Post $post, Request $request)
    {
        if ($request->ajax()) {
            $comments = (new ForumRespository)->getComments($post);

            return response()->json([
                'url' => route('show.comments',
                    array_merge([
                        $post->id,
                    ], $request->all())),
                'comments' => (new Serializer)->comments($comments),
            ]);

        }

        return view('forum.comments')->withPost(
            (new Serializer)->posts([$post])[0]
        );
    }
}
