<?php

namespace App\Http\Controllers\Api;

use App\Models\Parcel;
use App\Models\Subscribe;
use App\Models\Transaction;
use Barryvdh\Debugbar\Controllers\BaseController;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PayPalController extends BaseController
{
    public function createPayment(Request $request)
    {
        $client = new Client();
        $amount = $request->parcel == 'all' ? prices()['all'] : prices()['parcel'];

        $response = $client->request('POST', env('PAYPAL_API'),
            [
                'auth' => [env('PAYPAL_CLIENT_ID'), env('PAYPAL_SECRET')],
                'json' => [
                    "intent" => "sale",
                    "payer" => [
                        "payment_method" => "paypal",
                    ],
                    "transactions" => [
                        [
                            "amount" => [
                                "total" => $amount,
                                "currency" => "USD",
                            ],
                        ],
                    ],
                    "redirect_urls" => [
                        "return_url" => env('APP_URL'),
                        "cancel_url" => env('APP_URL'),
                    ],
                ],
            ]
        );

        return response()->json(json_decode($response->getBody()->getContents()));
    }

    public function executePayment(string $paymentID, string $payerID, Request $request)
    {
        $client = new Client();
        $url = env('PAYPAL_API') . $paymentID . '/execute/';
        $amount = $request->parcel == 'all' ? prices()['all'] : prices()['parcel'];

        $response = $client->request('POST', $url,
            [
                'auth' => [env('PAYPAL_CLIENT_ID'), env('PAYPAL_SECRET')],
                'json' => [
                    "payer_id" => $payerID,
                    "transactions" => [
                        [
                            "amount" => [
                                "total" => $amount,
                                "currency" => "USD",
                            ],
                        ],
                    ],
                ],
            ]
        );

        $userId = $request->user()->id;
        if ($request->parcel == 'all') {
            $subscribe = new Subscribe;
            $subscribe->user_id = $userId;
            $subscribe->ended_at = Carbon::now()->addMonths(1);
            $subscribe->save();
        } else {
            $transaction = Transaction::whereParcelId($request->parcel)
                ->whereUserId($userId)
                ->first();

            if (!$transaction) {
                $transaction = new Transaction;
                $transaction->user_id = $userId;
                $transaction->parcel_id = $request->parcel;
            }
            $transaction->in_wishlist = false;
            $transaction->done = 1;
            $transaction->thread = Parcel::find($request->parcel)->thread;
            $transaction->save();
        }

        return response()->json(json_decode($response->getBody()->getContents()));
    }
}
