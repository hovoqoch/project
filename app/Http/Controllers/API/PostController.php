<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Rate;
use App\Models\Supplier;
use App\Serializer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addPost(Request $request)
    {
        $post = Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'tender_id' => $request->tender,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json([
            'post' => (new Serializer)->posts([$post])[0],
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editPost(Post $post, Request $request)
    {
        $post->fill([
            'title' => $request->title,
            'body' => $request->body,
            'tender_id' => $request->tender,
            'user_id' => Auth::user()->id,
        ])->save();

        return response()->json([
            'post' => (new Serializer)->posts([$post])[0],
        ]);
    }

    public function getSuppliers()
    {
        $suppliers = Supplier::selectRaw('id, name')->get();
        return response()->json([
            'suppliers' => $suppliers,
        ]);
    }

    public function addComment(Post $post, Request $request)
    {
        $comment = $post->comments()->create([
            'body' => $request->body,
            'post_id' => $request->post,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json([
            'comment' => (new Serializer)->comments([$comment])[0],
        ]);
    }

    public function editComment(Post $post, Comment $comment, Request $request)
    {
        $comment->body = $request->body;
        $comment->save();

        return response()->json([
            'comment' => (new Serializer)->comments([$comment])[0],
        ]);
    }

    public function deleteComment(Post $post, Comment $comment)
    {
        $comment->delete();
        return response()->json([]);
    }

    public function deletePost(Post $post)
    {
        $post->delete();
        return response()->json([]);
    }

    public function votePost(Post $post, Request $request)
    {
        $vote = Auth::user()->voteComments()
            ->whereSubjectableId($post->id)
            ->first();

        if (!$vote) {
            $vote = new Rate;
            $vote->user_id = Auth::user()->id;
            $vote->subjectable_id = $post->id;
            $vote->subjectable_type = Post::class;
        }

        $vote->value = $request->value;
        $vote->save();

        $post->rates += $request->rate;
        $post->save();

        return response()->json([
            'rates' => $post->rates,
        ]);
    }
}
