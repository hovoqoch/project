<?php

namespace App\Http\Controllers\Api\My;

use App\Http\Controllers\Controller;
use App\Services\TransactionService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function addOrRemoveWishlist(string $thread, Request $request)
    {
        $this->transactionService->addOrRemoveWishlist(
            $request->user()->id,
            $thread,
            $request->get('in_wishlist', false)
        );

        return response()->json(['message' => 'Success'], 201);
    }

    public function makePurchases(Request $request)
    {
        try {
            $userId = $request->user()->id;
            $action = $request->action;
            $service = $this->transactionService;
            if (!method_exists($service, $action)) {
                return response()->json(['message' => 'Action not found'], 404);
            }
            if (!$service->$action($request)) {
                return response()->json(['message' => 'We can not process the action'], 422);
            }
            return response()->json(['message' => 'Action has been completed'], 201);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'exception' => (new \ReflectionClass($e))->getShortName(),
            ], 403);
        }
    }
}
