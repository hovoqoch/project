<?php

namespace App\Http\Controllers\Api;

use App\Events\ReportAdded;
use App\Events\ReportDeleted;
use App\Events\ReportRated;
use App\Http\Controllers\Controller;
use App\Models\Parcel;
use App\Models\Post;
use App\Models\Rate;
use App\Models\Tender;
use App\Models\User;
use App\Serializer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;

class FrontController extends Controller
{
    private $user;

    private $hasActiveSubscribe = false;

    private $purchases = [];

    public function uploadMedia(Parcel $parcel, Request $request)
    {
        foreach ($request->file('files', []) as $file) {
            $collection = substr($file->getMimeType(), 0, 5) == "image"
            ? 'images'
            : 'files';

            $parcel->addMedia($file)->toMediaCollection($collection, 'media');
            $parcel->save();
        }

        if ($parcel->hasFile()) {
            $parcel->is_draft = false;
            $parcel->save();
            Auth::user()->addBonus(50, [
                'type' => 'POINTS',
                'action' => [
                    'for' => 'UPLOAD_FILE #' . $parcel->id,
                    'effect' => 'ADD',
                ],
            ]);

            event(new ReportAdded($parcel));
        }

        return response()->json(['success']);
    }

    public function vote(Parcel $parcel, Request $request)
    {
        $rate = Rate::whereUserId($request->user()->id)
            ->whereSubjectableId($parcel->id)
            ->whereSubjectableType(Parcel::class)
            ->whereType($request->type)
            ->first();

        if ($rate) {
            $rate->value = $request->value;
            $rate->save();
        } else {
            Rate::create([
                'subjectable_type' => Parcel::class,
                'subjectable_id' => $parcel->id,
                'owner_id' => $parcel->user_id,
                'type' => $request->type,
                'value' => $request->value,
                'user_id' => $request->user()->id,
            ]);
        }

        event(new ReportRated($parcel));

        return response()->json([
            'up' => $parcel->votes($request->type, 1),
            'down' => $parcel->votes($request->type, -1),
        ]);
    }

    public function removeMedia(Parcel $parcel, Media $media)
    {
        $media->delete();

        if (!$parcel->hasFile()) {
            $parcel->is_draft = true;
            $parcel->save();

            Auth::user()->deactivateBonus(50, [
                'type' => 'POINTS',
                'action' => [
                    'for' => 'REMOVE_FILE #' . $parcel->id,
                    'effect' => 'SUBTRACT',
                ],
            ]);

            event(new ReportDeleted($parcel));
        }

        return response()->json(['success']);
    }

    public function getSuppliers(Request $request)
    {
        $suppliers = Tender::selectRaw('distinct(supplier_id)')
            ->with(['supplier' => function ($q) {
                $q->selectRaw('suppliers.id as id, suppliers.name as title');
            }])
            ->status($request->status)
            ->get();
        return response()->json(compact('suppliers'));
    }

    public function getTenders(Request $request)
    {
        $tenders = Tender::select('id', 'title')
            ->status($request->status)
            ->supplier($request->supplier)
            ->get();
        return response()->json(compact('tenders'));
    }

    private function status(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->status) {
            case "closed":
                $status = " and date(tenders.ended_at) < '$today'";
                break;
            case "active":
                $status = " and date(tenders.ended_at) >= '$today'";
                break;
            case "upcoming":
                $status = " and date(tenders.started_at) > '$today'";
                break;
        }

        return $status;
    }

    public function search(Request $request)
    {
        $ids = Tender::select('id')
            ->status($request->status)
            ->supplier($request->supplier)
            ->country($request->country)
            ->get()
            ->toArray();

        if (count($ids) == 0) {
            return response()->json([
                'url' => route('search', $request->all()),
                'parcels' => [],
            ]);
        }
        $ids = Parcel::live()
            ->selectRaw("max(`id`) as id")
            ->whereIn('tender_id', $ids)
            ->colorByEye($request->color_by_eye)
            ->colorByMachine($request->color_by_machine)
            ->flourscene($request->flourscene)
            ->size($request->only('size_from', 'size_to'))
            ->valuation($request->only('valuation_from', 'valuation_to'))
            ->groupBy('thread')
            ->get();

        $parcels = Parcel::with(['media', 'tender'])->live()->whereIn('id', $ids)
            ->paginate(10);

        return response()->json([
            'url' => route('search', $request->all()),
            'parcels' => (new Serializer)->parcel($parcels),
        ]);
    }

    public function getThread(Parcel $parcel)
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addPost(Request $request)
    {
        Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'tender_id' => $request->tender,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json([]);
    }
}
