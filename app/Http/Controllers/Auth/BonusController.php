<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Log;
use App\Models\Parcel;
use App\Models\Subscribe;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BonusController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $bonuses = Log::whereUserId(Auth::user()->id)
            ->whereType('POINTS')
            ->get();

        return view('bonus.index', compact('bonuses'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subtract(Request $request)
    {
        $limit = $request->price / prices()['parcel'];
        $userId = Auth::user()->id;

        $bonuses = Bonus::whereUserId($userId)
            ->whereIsActive(true)
            ->orderBy('id')
            ->limit($limit)
            ->get();

        foreach ($bonuses as $bonus) {
            $bonus->is_active = false;
            $bonus->save();
        }

        if ($request->subscribe) {
            $subscribe = new Subscribe;
            $subscribe->user_id = $userId;
            $subscribe->ended_at = Carbon::now()->addMonths(1);
            $subscribe->save();
        }

        if ($request->parcel_id && $request->parcel_id != 'all') {
            $transaction = Transaction::whereUserId(Auth::user()->id)
                ->whereParcelId($request->parcel_id)
                ->first();

            if (!$transaction) {
                $transaction = new Transaction;
            }

            $transaction->in_wishlist = false;
            $transaction->user_id = $userId;
            $transaction->parcel_id = $request->parcel_id;
            $transaction->done = 1;
            $transaction->thread = Parcel::find($request->parcel_id)->thread;
            $transaction->save();
        }

        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    public function points()
    {
        return response()->json(['points' => Auth::user()->points]);
    }
}
