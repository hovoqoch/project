<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Tender;
use App\Serializer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    public function index(Request $request)
    {
        $tenders = Tender::with(['supplier'])->active()->get();
        $subscriptions = Auth::user()->subscribes()->get();
        $serializer = new Serializer;

        return view('subscription.index', compact('subscriptions', 'tenders', 'serializer'));
    }
}
