<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Parcel;
use App\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = Auth::user();
        $repo = new Repository;

        $reports = [
            'total' => Parcel::selectRaw('count(id) as total')->whereUserId($user->id)->first()->total,
            'live' => Parcel::selectRaw('count(id) as live')->live()->whereUserId($user->id)->first()->live,
            'like' => $repo->reportUserRates($user->id),
            'dislike' => $repo->reportUserRates($user->id, -1),
        ];

        return view('auth.profile', compact('user', 'reports'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $data = $request->only('name', 'phone', 'email');

        if ($request->get('password')) {
            $data['password'] = $request->get('password');
            $data['password_confirmation'] = $request->get('password_confirmation');
        }

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:100', 'unique:users,name,' . $user->id],
            'phone' => ['required', 'string', 'max:20', 'unique:users,phone,' . $user->id],
            'email' => ['required', 'string', 'email', 'max:100', 'unique:users,email,' . $user->id],
            'password' => ['sometimes', 'string', 'confirmed', 'min:8'],
        ]);

        if ($validator->fails()) {
            return redirect(route('profile.edit'))
                ->withErrors($validator)
                ->withInput();
        }

        $user->name = $data['name'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        if (isset($data['password'])) {
            $user->password = bcrypt($data['password']);
        }
        if ($photo = $request->file('photo')) {
            $user->addMedia($photo)->toMediaCollection('images', 'media');
        }

        $user->save();

        return redirect(route('profile.edit'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );

    }
}
