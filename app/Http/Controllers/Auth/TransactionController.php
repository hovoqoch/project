<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Parcel;
use App\Repository;
use App\Serializer;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class TransactionController extends Controller
{
    public function getWishlist(Request $request)
    {
        if ($request->ajax()) {
            $repo = new Repository;
            $parcels = $repo->getWishlist(Auth::user()->id);
            $parcels = (new Serializer)->parcel($parcels);

            return response()->json([
                'parcels' => $parcels,
            ]);
        }

        return view('my/wishlist');
    }

    public function getPurchases(Request $request)
    {
        if ($request->ajax()) {
            $repo = new Repository;
            $parcels = $repo->getPurchases(Auth::user()->id);
            $parcels = (new Serializer)->parcel($parcels);

            return response()->json([
                'parcels' => $parcels,
            ]);
        }

        return view('my/purchases');
    }
}
