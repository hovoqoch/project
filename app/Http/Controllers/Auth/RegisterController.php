<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\LoginToken;
use App\Models\User;
use App\Notifications\PasswordlessNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function register(Request $request)
    {
        $validator = Validator::make(
            $request->only('name', 'email', 'password'), [
                'name' => ['required', 'string', 'max:100', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
            ]
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $user = User::create($request->only('name', 'email', 'password'));
        event(new Registered($user));
        Auth::guard()->login($user);

        return response()->json(['success' => 'Success']);
    }

    protected function byEmail(Request $request)
    {
        $validator = Validator::make(
            $request->only('email'), [
                'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }

        $token = rand(100000, 999999);
        LoginToken::create([
            'email' => $request->email,
            'token' => $token,
        ]);
        $user = User::create([
            'name' => $request->email,
            'email' => $request->email,
            'password' => $token,
        ]);

        Notification::send($user, new PasswordlessNotification($token));
        return response()->json(['message' => 'Sweet - go check that email, yo.']);
    }

    protected function sendCode(Request $request)
    {
        $validator = Validator::make(
            $request->only('email'), [
                'email' => ['required', 'string', 'email', 'max:100', 'exists:users'],
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'errors' => ['code' => 'Something wrong happend!'],
            ], 422);
        }

        $token = rand(100000, 999999);
        LoginToken::whereEmail($request->email)->delete();
        LoginToken::create([
            'email' => $request->email,
            'token' => $token,
        ]);
        $user = User::whereEmail($request->email)->first();
        Notification::send($user, new PasswordlessNotification($token));

        return response()->json(['message' => 'Sweet - go check that email, yo.']);
    }
}
