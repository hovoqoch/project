<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AuthProvider;
use App\Models\LoginToken;
use App\Models\User;
use App\Services\DetectorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Socialite;

class LoginController extends Controller
{
    private $rules = [
        'email' => 'required|string',
        'password' => 'required|string',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub|Google.
     *
     * @param string $provider
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = AuthProvider::login($provider, $user->id);

        if (!$authUser) {
            $validator = Validator::make([
                'name' => $user->name ?? $user->nickname,
                'login_email' => $user->email,
            ], [
                'name' => ['required', 'string', 'max:100', 'unique:users'],
                'login_email' => ['required', 'string', 'email', 'max:100', 'unique:users,email'],
            ]);

            if ($validator->fails()) {
                return redirect('/login')->withErrors($validator);
            }

            $authUser = User::create([
                'name' => $user->name ?? $user->nickname,
                'email' => $user->email,
                'password' => bcrypt(str_random(30)),
            ]);

            $authUser->authProvider()->create([
                'provider' => $provider,
                'identifier' => $user->id,
            ]);
        }

        Auth::login($authUser, true);
        $this->setStatus(true);

        return redirect('/');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->only('email', 'password'), $this->rules
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        if ($user = $this->isLogged($request->email)) {
            return response()->json([
                'errors' => [
                    'email' => ['We detected you are already logged from ' . DetectorService::parse($user->device)],
                ],
            ], 403);
        }

        $user = Auth::guard()
            ->attempt($request->only('email', 'password'), $request->filled('remember'));

        if (!$user) {
            return response()->json([
                'errors' => [
                    'email' => [trans('auth.failed')],
                ],
            ], 422);
        }

        $request->session()->regenerate();
        $this->setStatus(true);
        return response()->json(['success' => 'Success']);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->setStatus(false);

        Auth::guard()->logout();
        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Set user status
     *
     * @param boolean $status
     * @return void
     */
    private function setStatus(bool $status = true)
    {
        $user = Auth::user();
        if (!$status) {
            $user->is_online = false;
        } else {
            $user->is_online = true;
            $user->device = DetectorService::detect(request()->server('HTTP_USER_AGENT'));
        }

        $user->save();
    }

    /**
     * Set user status
     *
     * @param boolean $status
     * @return void
     */
    private function isLogged(string $email)
    {
        return User::where('email', $email)
            ->where('is_online', true)
            ->first();
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    protected function verifyEmail(string $token)
    {
        $token = LoginToken::with(['user'])->whereToken($token)->first();

        if (!$token) {
            return response()->json([
                'errors' => ['code' => ['Invalid token']],
            ], 422);
        }

        $user = $token->user;
        $user->email_verified_at = Date::now();
        $user->save();
        Auth::login($user);
        $this->setStatus(true);
        request()->session()->regenerate();

        return response()->json(['Success']);
    }
}
