<?php

namespace App\Http\Controllers;

use App\Models\Parcel;
use App\Models\Tender;
use App\Repository;
use App\Serializer;
use Illuminate\Http\Request;

class TenderController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $serializer = new Serializer;

        $active = Tender::with(['parcels' => function ($q) {
            return $q->live();
        }])
            ->active()
            ->get();

        $upcoming = Tender::with(['parcels' => function ($q) {
            return $q->live();
        }])
            ->upcoming()
            ->get();

        $closed = Tender::with(['parcels' => function ($q) {
            return $q->live();
        }])
            ->closed()
            ->get();

        return view('tender.index')->with([
            'active' => $serializer->ourTender($active),
            'upcoming' => $serializer->ourTender($upcoming),
            'closed' => $serializer->ourTender($closed),
        ]);
    }

    public function show(Tender $tender, Request $request)
    {
        $serializer = new Serializer;
        if ($request->ajax()) {
            $ids = (new Repository)->lastReportIds($tender->id);
            $parcels = $serializer->parcel(
                Parcel::live()
                    ->whereIn('id', $ids)
                    ->paginate(10)
            );

            return response()->json([
                'parcels' => $parcels,
            ]);
        }

        $canDownloadTender = $serializer->canDownloadTender($tender);
        return view('tender.show', compact('tender', 'canDownloadTender'));
    }
}
