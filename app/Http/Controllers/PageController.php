<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(string $page)
    {
        if (view()->exists('pages.' . $page)) {
            return view('pages.' . $page);
        }

        abort(404);
    }
}
