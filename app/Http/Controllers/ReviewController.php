<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $subject = json_decode($request->subject);

        $review = new Review;
        $review->comment = $request->comment ?? '';
        $review->rate = $request->rate;
        $review->user_id = Auth::user()->id;
        $review->subjectable_id = $subject->id;
        $review->subjectable_type = 'App\\Models\\' . ucwords($subject->type);
        $review->save();

        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Saved !'])
            );
    }

    public function delete(Review $review)
    {
        $review->delete();
        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Deleted !'])
            );
    }

    public function update(Review $review, Request $request)
    {
        $review->comment = $request->comment ?? '';
        $review->rate = $request->rate;
        $review->save();

        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Saved !'])
            );
    }
}
