<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::all();

        return view('admin.supplier.index', compact('suppliers'));
    }

    public function create()
    {
        $supplier = new Supplier;

        return view('admin.supplier.create', compact('supplier'));
    }

    public function edit(Supplier $supplier)
    {
        return view('admin.supplier.edit', compact('supplier'));
    }

    private function saveOrUpdate(Request $request, Supplier $supplier)
    {
        $request->validate([
            'name' => 'required',
            'country' => 'required',
        ]);

        if ($request->has('image')) {
            if ($supplier->hasImage()) {
                $supplier->media()->delete();
            }

            $supplier->addMedia($request->image)->toMediaCollection('images', 'media');
        }

        $supplier->name = $request->name;
        $supplier->country = $request->country;
        $supplier->save();
    }

    public function store(Request $request)
    {
        $this->saveOrUpdate($request, new Supplier);

        return redirect(route('admin.supplier.index'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    public function update(Supplier $supplier, Request $request)
    {
        $this->saveOrUpdate($request, $supplier);

        return redirect(route('admin.supplier.index'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }
}
