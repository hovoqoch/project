<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use App\Models\Tender;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TenderController extends Controller
{
    public function index()
    {
        $tenders = Tender::with(['supplier'])->get();

        return view('admin.tender.index', compact('tenders'));
    }

    public function create()
    {
        $tender = new Tender;
        $suppliers = Supplier::selectRaw('name as title, id')->get()->toArray();

        return view('admin.tender.create', compact('tender', 'suppliers'));
    }

    public function edit(Tender $tender)
    {
        $suppliers = Supplier::selectRaw('name as title, id')->get()->toArray();

        return view('admin.tender.edit', compact('tender', 'suppliers'));
    }

    private function saveOrUpdate(Request $request, Tender $tender)
    {
        $request->validate([
            'title' => 'required',
            'supplier_id' => 'required',
            'started_at' => ['required', 'date_format:d/m/Y'],
            'bid_started_at' => ['required', 'date_format:d/m/Y'],
            'ended_at' => ['required', 'date_format:d/m/Y'],
            'bid_ended_at' => ['required', 'date_format:d/m/Y'],
            'country' => ['required'],
        ]);

        if ($request->has('image')) {
            if ($tender->hasImage()) {
                $tender->media()->delete();
            }

            $tender->addMedia($request->image)->toMediaCollection('images', 'media');
        }

        $tender->title = $request->title;
        $tender->supplier_id = $request->supplier_id;
        $tender->started_at = Carbon::createFromFormat('d/m/Y', $request->started_at);
        $tender->bid_started_at = Carbon::createFromFormat('d/m/Y', $request->bid_started_at);
        $tender->ended_at = Carbon::createFromFormat('d/m/Y', $request->ended_at);
        $tender->bid_ended_at = Carbon::createFromFormat('d/m/Y', $request->bid_ended_at);
        $tender->description = $request->description;
        $tender->country = $request->country;
        $tender->save();

        return $tender;
    }

    public function store(Request $request)
    {
        $tender = $this->saveOrUpdate($request, new Tender);

        event('tender.was.added', $tender);
        return redirect(route('admin.tender.index'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    public function update(Tender $tender, Request $request)
    {
        $tender = $this->saveOrUpdate($request, $tender);

        event('tender.was.updated', $tender);
        return redirect(route('admin.tender.index'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }
}
