<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ads;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function index()
    {
        $ads = Ads::get();
        return view('admin.ads.index', compact('ads'));
    }

    public function edit(Ads $ads)
    {
        return view('admin.ads.edit', compact('ads'));
    }

    public function update(Ads $ads, Request $request)
    {
        $this->saveOrUpdate($request, $ads);

        return redirect(route('admin.ads.index'))
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }

    private function saveOrUpdate(Request $request, Ads $ads)
    {
        $ads->active = $request->active;
        $ads->body = $request->body;
        $ads->page = $request->page;
        $ads->type = $request->type;
        $ads->save();
    }

}
