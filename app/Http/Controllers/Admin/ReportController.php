<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Parcel;

class ReportController extends Controller
{
    public function index()
    {
        $reports = Parcel::with(['media', 'user', 'tender'])->paginate(10);

        return view('admin.report.index', compact('reports'));
    }

    public function delete(Parcel $parcel)
    {
        $parcel->delete();
        return back()
            ->withStatus(
                json_encode(['type' => 'success', 'text' => 'Success done'])
            );
    }
}
