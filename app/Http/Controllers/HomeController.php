<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\Tender;
use App\Repositories\SupplierRespository;
use App\Repositories\UserRespository;
use App\Repository;
use App\Serializer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $repo = new UserRespository;
        $users = $repo->getBeforeEnding();

        $serializer = new Serializer;

        $mostDownload = $serializer->mostDownload(
            (new Repository)->mostDownload()
        );

        $tenders = Tender::with(['parcels' => function ($query) {
            return $query->live();
        }])->active()->get();

        $tenders = $serializer->ourTender($tenders);
        $suppliers = $serializer->supplier((new SupplierRespository)->getAll());

        $ads = null;
        if ($request->get('ads') == 'welcome') {
            $ads = Ads::whereType('welcome')
                ->wherePage('/')
                ->whereActive(true)
                ->first();
        }
        return view('home', compact('mostDownload', 'tenders', 'suppliers', 'ads'));
    }

    public function search(Request $request)
    {
        $request = count($request->all()) ? $request->all() : ['status' => 'active'];
        return view('search.index', compact('request'));
    }

    public function notifications(Request $request)
    {
        $notifications = Auth::user()->unreadNotifications()->get();

        return view('my.notifications', compact('notifications'));
    }

    public function markAsRead(Request $request)
    {
        Auth::user()->unreadNotifications->markAsRead();

        return back()->withStatus(
            json_encode(['type' => 'success', 'text' => 'Success'])
        );
    }

    public function speed(Request $request)
    {
        return view('test');
    }
}
