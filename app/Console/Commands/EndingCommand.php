<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Notifications\EndingNotification;
use App\Repositories\UserRespository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class EndingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:ending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification ending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = new UserRespository;
        $subscribes = $repo->getEnding();

        foreach ($subscribes as $subscribe) {
            $user = User::find($subscribe->user_id);
            Notification::send($user, new EndingNotification([
                'ended_at' => $subscribe->ended_at->toDateString(),
                'name' => $user->name,
                'subscribe' => [
                    'type' => $subscribe->presentType(),
                    'created_at' => $subscribe->created_at,
                    'ended_at' => $subscribe->ended_at,
                ],
            ]));
        }
    }
}
