<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        'App\Events\ReportAdded' => [
            // 'App\Listeners\ReportAddedListener',
            // 'App\Listeners\PointAddedListener',
        ],

        'App\Events\ReportDeleted' => [
            // 'App\Listeners\ReportDeletedListener',
            // 'App\Listeners\PointSubtractedListener',
        ],

        'App\Events\ReportRated' => [
            'App\Listeners\ReportRatedListener',
        ],

        'App\Events\ReportViewed' => [
            // 'App\Listeners\ReportViewedListener',
        ],

        'App\Events\TransactionAdded' => [
            'App\Listeners\TransactionAddedListener',
        ],
    ];

    protected $subscribe = [
        'App\Listeners\ReportDownloadedListener',
        // 'App\Listeners\ForumListener',
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
