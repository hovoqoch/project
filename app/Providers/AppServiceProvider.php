<?php

namespace App\Providers;

use App\Models\Parcel;
use App\Observers\ParcelObserver;
use App\Observers\PushSubscriptionObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use NotificationChannels\WebPush\PushSubscription;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Paginator::defaultView('vendor.pagination.bootstrap-4');
        View::share('PRICE', prices()['parcel']);
        View::share('SUBSCRIBE', false);
        Parcel::observe(ParcelObserver::class);
        PushSubscription::observe(PushSubscriptionObserver::class);
        // Tender::observe(TenderObserver::class);
        Schema::defaultStringLength(191);
    }
}
