<?php

/**
 * This file is part of safetrade app.
 *
 * @author Omar Makled <omar.makled@gmail.com>
 */

namespace App\Repositories;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Collection;

class SupplierRespository
{
    /**
     * Get users has monthly subscription.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return Supplier::with('media')->get();
    }
}
