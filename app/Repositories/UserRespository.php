<?php

/**
 * This file is part of safetrade app.
 *
 * @author Omar Makled <omar.makled@gmail.com>
 */

namespace App\Repositories;

use App\Models\Parcel;
use App\Models\Subscribe;
use App\Models\Tender;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class UserRespository
{
    /**
     * Get users has monthly subscription.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getMonthlySubscribers()
    {
        $today = date('Y-m-d');

        $ids = Subscribe::select('user_id')
            ->whereRaw("date(`ended_at`) > '$today'")
            ->pluck('user_id', 'user_id');

        return User::whereIn('id', $ids)->get();
    }

    /**
     * Get users has tender subscription.
     *
     * @param  \App\Models\Tender $tender
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getTenderSubscribers(Tender $tender)
    {
        $ids = Transaction::select('user_id')
            ->whereTenderId($tender->id)
            ->whereDone(true)
            ->pluck('user_id', 'user_id');

        return User::whereIn('id', $ids)->get();
    }

    /**
     * Get users has parcel subscription.
     *
     * @param  \App\Models\Parcel $parcel
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getParcelSubscribers(Parcel $parcel)
    {
        $ids = Transaction::select('user_id')
            ->whereThread($parcel->thread)
            ->whereDone(true)
            ->pluck('user_id', 'user_id');

        return User::whereIn('id', $ids)->get();
    }

    /**
     * Get users of thread.
     *
     * @param  \App\Models\Parcel $parcel
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getThread(Parcel $parcel)
    {
        $ids = Parcel::select('user_id')
            ->whereThread($parcel->thread)
            ->pluck('user_id', 'user_id');

        return User::whereIn('id', $ids)->get();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBeforeEnding()
    {
        $dates = [
            "'" . Carbon::now()->addDays(1)->toDateString() . "'",
            "'" . Carbon::now()->addDays(3)->toDateString() . "'",
            "'" . Carbon::now()->addDays(7)->toDateString() . "'",
        ];

        $dates = implode($dates, ',');

        return Subscribe::whereRaw("date(ended_at) in ($dates)")->get();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getEnding()
    {
        $today = date('Y-m-d');

        return Subscribe::whereRaw("date(ended_at) = '$today'")->get();
    }
}
