<?php

/**
 * This file is part of safetrade app.
 *
 * @author Omar Makled <omar.makled@gmail.com>
 */

namespace App\Repositories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Tender;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ForumRespository
{
    /**
     * Get users has monthly subscription.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBySupplier(?Int $supplierId)
    {
        $with = ['votes', 'user', 'comments', 'tender.supplier'];

        if (!$supplierId) {
            return Post::with($with)->orderByRaw('rates DESC')->paginate(5);
        }

        $ids = Tender::select('id')
            ->where('supplier_id', $supplierId)
            ->pluck('id', 'id')
            ->toArray();

        if (!$ids) {
            return new LengthAwarePaginator([], 0, 1);
        }

        return Post::with($with)->tenders($ids)->orderByRaw('rates DESC')->paginate(5);
    }

    public function getComments(Post $post)
    {
        return Comment::with(['user'])->wherePostId($post->id)->orderBy('id', 'desc')->paginate(10);
    }

    public function getUserVotes($userId)
    {
        return User::find($userId)
            ->voteComments()
            ->select('subjectable_id as id', 'value')
            ->get()
            ->pluck('value', 'id');
    }
}
