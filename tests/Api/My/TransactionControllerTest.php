<?php

namespace Tests\Api\My;

use App\Models\Bonus;
use App\Models\User;
use Tests\TestCase;

class TransactionControllerTest extends TestCase
{

    public function testAddToWishlist()
    {
        $user = User::first();
        $this->actingAs($user);

        $response = $this->post('/api/my/wishlist/1-a', [
            'in_wishlist' => true,
        ]);
        $response->assertStatus(201);

        $response = $this->post('/api/my/wishlist/1-a', [
            'in_wishlist' => false,
        ]);
        $response->assertStatus(201);
    }

    public function testMakePurchases()
    {
        $user = User::first();
        $this->actingAs($user);

        factory(Bonus::class, 1)->create([
            'user_id' => $user->id,
        ]);
        $response = $this->post('/api/my/purchases/1-a', [
            'method' => 'bonus',
        ]);
        $response->assertStatus(201);
    }

    public function testMakeSubscribe()
    {
        $user = User::first();
        $this->actingAs($user);

        factory(Bonus::class, 10)->create([
            'user_id' => $user->id,
        ]);
        $response = $this->post('/api/my/subscribe/year', [
            'method' => 'bonus',
        ]);
        $response->assertStatus(201);
    }

    public function testCanNotMakeSubscribe()
    {
        $user = User::first();
        $this->actingAs($user);

        factory(Bonus::class, 1)->create([
            'user_id' => $user->id,
        ]);
        $response = $this->post('/api/my/subscribe/year', [
            'method' => 'bonus',
        ]);
        $response->assertStatus(422);
    }

    public function testCanNotMakePurchases()
    {
        $user = User::first();
        $this->actingAs($user);

        $response = $this->post('/api/my/purchases/1-a', [
            'method' => 'bonus',
        ]);
        $response->assertStatus(422);
    }

    public function testMakePurchasesTenderByBonus()
    {
        $user = User::first();
        $this->actingAs($user);

        factory(Bonus::class, 10)->create([
            'user_id' => $user->id,
        ]);
        $response = $this->post('/api/my/subscribe/tender/1', [
            'method' => 'bonus',
        ]);
        $response->assertStatus(201);
    }
}
