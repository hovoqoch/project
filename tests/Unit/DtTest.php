<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use App\Services\Dt;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DtTest extends TestCase
{
    public function testIsUpcommingTest()
    {
        $startAt = Carbon::now()->addDays(1);
        $endAt = Carbon::now()->addDays(3);
        $status = Dt::status($startAt, $endAt);
        $this->assertEquals('upcommnig', $status);
    }

    public function testIsClosedTest()
    {
        $startAt = Carbon::now()->addDays(-3);
        $endAt = Carbon::now()->addDays(-1);

        $status = Dt::status($startAt, $endAt);
        $this->assertEquals('closed', $status);
    }

    public function testIsActiveTest()
    {
        $startAt = Carbon::now();
        $endAt = Carbon::now();

        $status = Dt::status($startAt, $endAt);
        $this->assertEquals('active', $status);
    }
}
