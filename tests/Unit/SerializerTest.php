<?php

namespace Tests\Unit;

use App\Serializer;
use Tests\TestCase;

class SerializerTest extends TestCase
{
    public function testPostVoted()
    {
        $voted = [1 => 0, 2 => 1, 3 => -1];

        $serializer = new Serializer;

        $this->assertEquals(-1, $serializer->postVoted($voted, 3));
    }
}
