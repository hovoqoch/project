export const home = {
    lazyLoad: "ondemand",
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
}
export const parcel = {
    lazyLoad: "ondemand",
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
}
