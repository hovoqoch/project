/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import Vuex from 'vuex'

window.Inputmask = require('inputmask');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(Vuex);
Vue.component("text-group", require("./components/TextGroup.vue").default);
Vue.component(
    "text-area-group",
    require("./components/TextAreaGroup.vue").default
);
Vue.component("number-group", require("./components/NumberGroup.vue").default);
Vue.component("email-group", require("./components/EmailGroup.vue").default);
Vue.component(
    "password-group",
    require("./components/PasswordGroup.vue").default
);
Vue.component(
    "password-confirmation-group",
    require("./components/PasswordConfirmationGroup.vue").default
);
Vue.component("form-group", require("./components/FormGroup.vue").default);
Vue.component(
    "submit-button",
    require("./components/SubmitButton.vue").default
);
Vue.component(
    "checkbox-group",
    require("./components/CheckBoxGroup.vue").default
);
Vue.component(
    "select-group",
    require("./components/SelectGroup.vue").default
);
Vue.component(
    "toaster",
    require("./components/Toaster.vue").default
);
Vue.component(
    "wishlist",
    require("./components/WishList.vue").default
);
Vue.component(
    "cart",
    require("./components/Cart.vue").default
);
Vue.component(
    "rate",
    require("./components/Rate.vue").default
);
Vue.component("date-group", require("./components/DateGroup.vue").default);
Vue.component("country-list", require("./components/CountryList.vue").default);
Vue.component("uppy", require("./components/Uppy.vue").default);
Vue.component("delete-media", require("./components/DeleteMedia.vue").default);
Vue.component("form-search", require("./components/FormSearch.vue").default);
Vue.component("paypal", require("./components/PayPal.vue").default);
Vue.component("file-group", require("./components/FileGroup.vue").default);
Vue.component("vote", require("./components/Vote.vue").default);
Vue.component("search", require("./components/Search.vue").default);
Vue.component("parcel", require("./components/Parcel.vue").default);
Vue.component("thread-image", require("./components/ThreadImage.vue").default);
Vue.component("like", require("./components/Like.vue").default);
Vue.component("pay-with", require("./components/PayWith.vue").default);
Vue.component("points", require("./components/Points.vue").default);
Vue.component("buy-points", require("./components/BuyPoints.vue").default);
Vue.component("payment-modal", require("./components/PaymentModal.vue").default);
Vue.component("parcels", require("./components/Parcels.vue").default);
Vue.component("most-download", require("./components/MostDownload.vue").default);
Vue.component("our-tender", require("./components/OurTender.vue").default);
Vue.component("simple-slider", require("./components/SimpleSlider.vue").default);
Vue.component("parcel-slider", require("./components/ParcelSlider.vue").default);
Vue.component("country-slider", require("./components/CountrySlider.vue").default);
Vue.component("forum-category", require("./components/ForumCategory.vue").default);
Vue.component("post-form", require("./components/PostForm.vue").default);
Vue.component("post-list", require("./components/PostList.vue").default);
Vue.component("post-comment", require("./components/PostComment.vue").default);
Vue.component("delete-comment", require("./components/DeleteComment.vue").default);
Vue.component("delete-post", require("./components/DeletePost.vue").default);
Vue.component("post-vote", require("./components/PostVote.vue").default);
Vue.component("post", require("./components/Post.vue").default);
Vue.component("auth", require("./components/Auth.vue").default);
Vue.component("plans", require("./components/Plans.vue").default);
Vue.component("ads", require("./components/Ads.vue").default);
Vue.component("settings", require("./components/Settings.vue").default);
Vue.component("push-notification", require("./components/PushNotification.vue").default);
Vue.component("form-upload", require("./components/FormUpload.vue").default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.directive('date-mask', {
    bind: function (el) {
        new Inputmask({
            alias: 'datetime',
            inputFormat: 'dd/mm/yyyy'
        }).mask(el);
    },
});

Vue.directive('selectpicker', {
    bind: function (el) {
        console.log(this);
        // $(el).selectpicker();
        // $('.selectpicker').selectpicker();
        // console.log($(this.el))
        // $(this).selectpicker();

        // $(this.el).on('change', function (e) {
        // this.set($(this.el).val());
        // }.bind(this));
    },
    update(newValue) {
        // $(this.el).val(newValue);
        // $(this.$el).selectpicker('val', newValue);
    },
    unbind() {
        // $(this.el).selectpicker('destroy');
    }
});
import Toasted from 'vue-toasted';

Vue.use(Toasted)

import axios from "axios";
import Slick from 'vue-slick';
import "lazysizes/plugins/bgset/ls.bgset";
import 'lazysizes';

// import paypal from 'paypal-checkout';
const store = new Vuex.Store({
    state: {
        payment: null,
        subtractedPoints: 0,
        showPoints: false,
        auth: 'Register',
        email: null,
        points: [{
                price: 200,
                text: "Buy 250 points with $200",
                points: 250
            },
            {
                price: 400,
                text: "Buy 500 points with $400",
                points: 500
            },
            {
                price: 600,
                text: "Buy 750 points with $600",
                points: 750
            }
        ]
    },
    mutations: {
        setPayment(state, payment) {
            state.payment = payment
            state.subtractedPoints = payment.payload.points
        },
        setShowPoints(state, value) {
            state.showPoints = value
        },
        setAuth(state, type) {
            state.auth = type
        },
        setEmail(state, email) {
            state.email = email
        }
    },
    actions: {
        buyWith(context, method) {
            let payment = context.getters.getPayment;
            const URL = '/api/my/purchases'
            return axios
                .post(URL, {
                    method: method,
                    ...payment.payload
                })
        }
    },
    getters: {
        getPayment: (state) => {
            return state.payment;
        }
    }
})
const app = new Vue({
    el: "#app",
    components: {
        Slick
    },
    data() {
        return {
            slickOptions: {
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4
            },
        };
    },
    store,
    mounted() {
        $(".datepicker").datepicker({
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var action = button.data('url')
            var modal = $(this)
            modal.find('form').attr('action', action)
        })
        $('#reviewModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget),
                action = button.data('action'),
                method = button.data('method'),
                comment = button.data('comment'),
                modal = $(this);

            modal.find('form').attr('action', action)

            modal.find('input[name="_method"]')
                .val(method)

            modal.find('textarea[name="comment"]')
                .val(comment)
        })
        $('#authModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var action = button.data('auth') || 'Login'
            store.commit('setAuth', action)
        })
    },
    created() {
        // paypal.Button.render({
        //     env: "sandbox",
        //     style: {
        //         color: 'silver',
        //         shape: 'pill',
        //         label: 'pay',
        //         height: 40
        //     },
        //     payment: (data, actions) => {
        //         return actions.request
        //             .post("/api/create-payment", {
        //                 parcel: store.getters.getParcel
        //             })
        //             .then(function (res) {
        //                 return res.id;
        //             });
        //     },
        //     onAuthorize: (data, actions) => {
        //         return actions.request
        //             .post(`/api/execute-payment/${data.paymentID}/${data.payerID}`, {
        //                 parcel: store.getters.getParcel
        //             })
        //             .then((res) => {
        //                 this.$toasted.info("Success", {
        //                     theme: "toasted-primary",
        //                     className: "bg-info",
        //                     position: "bottom-right",
        //                     duration: 5000
        //                 });
        //                 location.reload();
        //             });
        //     }
        // }, ".paypal-button")
    }
});
