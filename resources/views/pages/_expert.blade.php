<div class="p-4 border">
    <div class="font-weight-bolder parcel__price text-primary float-right">${{prices()['all']}}</div>
    <h4 class="mb-4">Expert</h4>
    <p>
        Monthly Subscription, <strong>All Access</strong><br>
        Experts who need full access
    </p>
    <ul class="list-unstyled">
        <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Machine.</li>
        <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Eye.</li>
        <li class="mb-2"><img src="/images/check.png" alt=""> Fluorescence.</li>
        <li class="mb-2"><img src="/images/check.png" alt=""> Pictures of Stone/Parcel.</li>
        <li class="mb-2"><img src="/images/check.png" alt=""> Sarin Mapping File.</li>
    </ul>
    <div class="justify-content-center row">
        @guest
        <a href="{{route('login')}}" class="btn btn-primary btn-block">Get Started</a>
        @else
        @if(! Auth::user()->hasActiveSubscribe())
        <pay-with :block="true" :title="'Get started'" :payment="{{json_encode([
            'payload' => [
                'action' => 'buyMonth'
            ],
            'paypal' => [
                'description' => "Subscribe for month",
                'amount' => [
                    'currency_code' => 'USD',
                    'value' => appPackages()['month']['price']
                ]
            ]
        ])}}">
        </pay-with>
        @else
        <a href="{{route('my.subscription')}}" class="btn btn-success btn-block">Congratulations you already have a
            subscription</a>
        @endif
        @endif
    </div>
</div>
