<section class="mb-5">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-3 {{ $h3 ?? ''}}">{{ __('Features!')}}</h3>
                <p class="w-50">Sarin is a platform where you can share your reports and buy others from authors.</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-3">
                <img data-src="/images/tag4.png" class="lazyload" alt="">
                <p>View multiple expert reports on all available stones for any mine.</p>
            </div>
            <div class="col-lg-3">
                <img data-src="/images/tag1.png" class="lazyload" alt="">
                <p>Save time: Complete viewing of tender in hours, not days.</p>
            </div>
            <div class="col-lg-3">
                <img data-src="/images/tag2.png" class="lazyload" alt="">
                <p> Avoid expensive mistakes</p>
            </div>
            <div class="col-lg-3">
                <img data-src="/images/tag3.png" class="lazyload" alt="">
                <p>No need to carry expensive machinery or travel with planners</p>
            </div>
        </div>
    </section>