@extends('layouts.app')

@section('content')
<div class="container">
    <div class="mb-4 row">
        <div class="col-4">
            <h1 class="text-primary h2">{{__('Frequently Asked Questions')}}</h1>
            <p class="text-muted">{{__('We are here to help you!')}}</p>
        </div>
    </div>
    <div class="row">
        @for ($i = 0; $i < 5; $i++)
        <div class="box col-12 mb-5 p-3 border">
            <div class="">
                <button 
                    class="show btn btn-sm collapse float-right" 
                    data-toggle="collapse" 
                    data-target="#section-{{$i}}">
                </button>
                <h3 class="h5">Lorem ipsum dolor sit.</h3>
            </div>
            <div class="about collapse show collapsed pt-5 pr-5" id="section-{{$i}}">
                <h3>Lorem.</h3>
                <p>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rerum perspiciatis, soluta consectetur suscipit saepe a id provident labore est perferendis quibusdam ratione quasi? Error ducimus quaerat officiis. Autem, impedit harum!
                </p>
                <h3>Lorem.</h3>
                <p>
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rerum perspiciatis, soluta consectetur suscipit saepe a id provident labore est perferendis quibusdam ratione quasi? Error ducimus quaerat officiis. Autem, impedit harum!
                </p>
            </div>
        </div>
        @endfor
    </div>
</div>
@endsection
