@extends('layouts.app')

@section('content')
<div class="container">
    <div class="mb-5 row">
        <div class="col-4 pt-5">
            <h1 class="h2 mb-3 text-primary">{{__('Contact us')}}</h1>
            <p class="text-muted">{{__('Any questions, acquires or issues, tell us! ')}}</p>
        </div>
        <div class="col-8">
            <img src="/images/contact-us.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="">Full name</label>
                <input type="text" class="form-control" id="" name="name">
            </div>
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="form-control" id="" name="email">
            </div>
            <div class="form-group">
                <label for="">Phone</label>
                <input type="text" class="form-control" id="" name="phone">
            </div>
            <div class="form-group">
                <label for="">Your message</label>
                <textarea name="message" id="" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Send message</button>
            </div>
        </div>
        <div class="col-6">
            <div class="keep-in-touch ml-5 mt-5 p-5">
                <h4>Keep in touch</h4>
                <h5 class="font-weight-light h6 w-50">We love to always hear from You! </h5>
                <ul class="list-unstyled">
                    <li class="mb-2"><i class="fa fa-home mr-1"></i> United arab emirates, 23 street, 3 building.</li>
                    <li class="mb-2"><i class="fa fa-phone-alt mr-1"></i>002 25698356</li>
                    <li class="mb-2"><i class="fa fa-envelope mr-1"></i>Sarin@gmail.com</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
