@extends('layouts.app')

@section('content')
<div class="container">
    <section class="mb-5">
        <div class="row">
            <div class="col-lg-6 mb-3 mb-lg-0">
                <img class="img-fluid w-100" src="/images/logo-header.png" alt="">
            </div>
            <div class="align-items-center col-lg-5 d-flex offset-lg-1">
                <div>
                    <h3 class="mb-3"> A guide for how to use SafeTrade Reports</h3>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Necessitatibus animi corrupti inventore
                        culpa sint ea?</p>
                    <p><a class="btn btn-danger" href="#" data-toggle="modal"
                        data-target="#authModal">Sign up to get started</a></p>
                    <p><a class="btn btn-outline-primary" href="#learn-more">Learn more</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-5">
        <div class="line" id="learn-more"></div>
        <h3 class="mb-5 mt-5 text-center">{{trans('text.buyer.steps_title')}}</h3>
        <div class="row">
            @foreach (trans('text.buyer.steps') as $step)
                <div class="col-lg-3">
                    <svg viewBox="0 0 32 32" role="presentation" aria-hidden="true" focusable="false"
                        class="mb-3 icon__step">
                        <path
                            d="m16 31c-8.28 0-15-6.72-15-15s6.72-15 15-15 15 6.72 15 15-6.72 15-15 15m0-31c-8.84 0-16 7.16-16 16s7.16 16 16 16 16-7.16 16-16-7.16-16-16-16m5.71 12.29c.39.39.39 1.02 0 1.41l-6 6c-.39.39-1.02.39-1.41 0l-3-3c-.39-.39-.39-1.02 0-1.41s1.02-.39 1.41 0l2.29 2.29 5.29-5.29c.39-.39 1.02-.39 1.41 0">
                        </path>
                    </svg>
                    <h6 class="mb-3">{{$step['title']}}</h6>
                    <p>{!! $step['body'] !!}</p>
                </div>                
            @endforeach
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <h3 class="mb-5 mt-5 text-center">{{trans('text.buyer.cards_title')}}</h3>
        <div class="row">
            @foreach (trans('text.buyer.cards') as $content)
            <div class="col-lg-6 mb-5">
                <div class="text-center">
                    <img class="img-fluid mb-4 img__200 lazyload" data-src="{{$content['src']}}" alt="">
                </div>
                <h6 class="mb-3">{{$content['title']}}</h6>
                <p>{{$content['body']}}</p>
            </div>
            @endforeach
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <h3 class="mb-5 mt-5 text-center">{{trans('text.buyer.questions_title')}}</h3>
        <ul class="list-group w-100">
            @foreach (trans('text.buyer.questions') as $index => $question)
            <li class="list-group-item">
                <h6>
                    <a href="#faq-{{$index}}" data-toggle="collapse" role="button" aria-expanded="false"
                        aria-controls="faq-{{$index}}" class="text-dark d-block collapse faq">
                        <i class="fa float-right"></i>
                        {{$question['title']}}
                    </a>
                </h6>
                <div class="collapse mt-3" id="faq-{{$index}}">
                    {!!$question['body']!!}
                </div>
            </li>
            @endforeach
        </ul>
    </section>
</div>
@endsection
