@extends('layouts.app')

@section('content')
<div class="container">
    <section class="mb-5">
        <div class="row">
            <div class="col-lg-6 mb-3 mb-lg-0">
                <img class="img-fluid w-100" src="/images/earn.svg" alt="">
            </div>
            <div class="align-items-center col-lg-5 d-flex offset-lg-1">
                <div>
                    <h3 class="mb-3">{{trans('text.earn.title')}}</h3>
                    <p>{{trans('text.earn.body')}}</p>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <div class="row row mt-5">
            @foreach (trans('text.earn.cards') as $card)
            <div class="col-lg-6">
                <h6 class="mb-3">{{$card['title']}}</h6>
                <p>
                    {{$card['body']}}
                </p>
            </div>
            @endforeach
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <h3 class="mb-5 mt-5 text-center">{{trans('text.earn.steps_title')}}</h3>
        <div class="row">
            @foreach (trans('text.earn.steps') as $step)
            <div class="col-lg-4">
                <svg viewBox="0 0 32 32" role="presentation" aria-hidden="true" focusable="false"
                    class="mb-3 icon__step">
                    <path
                        d="m16 31c-8.28 0-15-6.72-15-15s6.72-15 15-15 15 6.72 15 15-6.72 15-15 15m0-31c-8.84 0-16 7.16-16 16s7.16 16 16 16 16-7.16 16-16-7.16-16-16-16m5.71 12.29c.39.39.39 1.02 0 1.41l-6 6c-.39.39-1.02.39-1.41 0l-3-3c-.39-.39-.39-1.02 0-1.41s1.02-.39 1.41 0l2.29 2.29 5.29-5.29c.39-.39 1.02-.39 1.41 0">
                    </path>
                </svg>
                <h6 class="mb-3">{{$step['title']}}</h6>
                <p>{{$step['content']}}</p>
            </div>
            @endforeach
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <h3 class="mb-5 mt-5 text-center">{{__('text.earn.How earnings are calculated.title') }}</h3>
        <div class="row">
            @foreach (__('text.earn.How earnings are calculated.contents') as $content)
            <div class="col-lg-6 mb-5">
                <div class="text-center">
                    <img class="img-fluid mb-4 img__200 lazyload" data-src="{{$content['src']}}" alt="">
                </div>
                <h6 class="mb-3">{{$content['title']}}</h6>
                <p>{{$content['body']}}</p>
            </div>
            @endforeach
        </div>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <h3 class="mb-5 mt-5 text-center">{{trans('text.earn.faq.title')}}</h3>
        <ul class="list-group w-100">
            @foreach (trans('text.earn.faq.contents') as $index => $question)
            <li class="list-group-item">
                <h6>
                    <a href="#faq-{{$index}}" data-toggle="collapse" role="button" aria-expanded="false"
                        aria-controls="faq-{{$index}}" class="text-dark d-block collapse faq">
                        <i class="fa float-right"></i>
                        {{$question['title']}}
                    </a>
                </h6>
                <div class="collapse mt-3" id="faq-{{$index}}">
                    {!!$question['body']!!}
                </div>
            </li>
            @endforeach
        </ul>
    </section>
    <section class="mb-5">
        <div class="line"></div>
        <div class="mt-5 text-muted">
            <p>
                Certain requirements and features vary by country, region, and city.
            </p>
            <p class="mb-1">
                * Promotions are coming soon; Dynamic Pricing is coming soon; Cash out is. coming soon
            </p>
            <p class="mb-1">
                ** Information provided on this page is for informational purposes only and does not guarantee earnings. Earnings structures may differ by city. Always check your city specific website for the most accurate details on delivery fares in your city.
            </p>
        </div>
    </section>
</div>
@endsection
