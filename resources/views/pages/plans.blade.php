@extends('layouts.app')
@section('content')
<div class="container">
    <section class="mb-5">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-3 text-primary">{{ __('Plans')}}</h3>
                <p class="">Sarin is a platform where you can share your reports and buy others from authors.</p>
            </div>
        </div>
    </section>
</div>
<plans 
    :links="{{json_encode(settings('plans'))}}"
    :costs="{{json_encode(trans('text.plans.cost'))}}"
    :features="{{json_encode(trans('text.plans.features'))}}">
</plans>
@endsection
