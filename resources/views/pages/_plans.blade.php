@extends('layouts.app')

@section('content')
@php($PRICE = prices()['all'])
@php($SUBSCRIBE = true)
<div class="container pricing">
    @component('pages/_features')
    @slot('h3')
    text-primary
    @endslot
    @endcomponent
    <div class="row">
        <div class="col-lg offer">
            <div class="p-4 border">
                <h4 class="mb-4">Individual</h4>
                <p>
                    Pay As You Go, <strong>Complete Freedom</strong><br>
                    For those who view less than 10 parcels a month
                </p>
                <ul class="list-unstyled">
                    <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Machine.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Eye.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Fluorescence.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Pictures of Stone/Parcel.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Sarin Mapping File.</li>
                </ul>
                <div class="justify-content-center row">
                    @guest
                    <a href="{{route('login')}}" class="btn btn-primary btn-block">Get Started</a>
                    @else
                    <a href="{{route('home')}}" class="btn btn-primary btn-block">Get Started</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg offer">
            <div class="p-4 border">
                <h4 class="mb-4">Points</h4>
                <p>
                    Pay As You Go, <strong>Best Deal</strong><br>
                    Buy points to use it
                </p>
                <ul class="list-unstyled">
                    <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Machine.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Color Reading by Eye.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Fluorescence.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Pictures of Stone/Parcel.</li>
                    <li class="mb-2"><img src="/images/check.png" alt=""> Sarin Mapping File.</li>
                </ul>
                <div class="justify-content-center row">
                    @guest
                    <a href="{{route('login')}}" class="btn btn-primary btn-block">Get Started</a>
                    @else
                    <pay-with :block="true" :title="'Get started'" :payment="{{json_encode([
                            'payload' => [
                                'action' => 'buyPoints',
                                'points' => appPackages()['points'][0]['points'] 
                            ],
                            'paypal' => [
                                'description' => "Buy points",
                                'amount' => [
                                    'currency_code' => 'USD',
                                    'value' => appPackages()['points'][0]['price']
                                ]
                            ]
                        ])}}">
                    </pay-with>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg offer">
            @include('pages/_expert')
        </div>
    </div>
</div>
@endsection
