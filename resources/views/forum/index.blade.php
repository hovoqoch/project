@extends('layouts.app')

@section('bg')
bg-light
@endsection

@section('content')
<div class="container">
    <post-list :request="{{json_encode(request()->all())}}"></post-list>
</div>
@endsection
