@extends('layouts.app')

@section('bg')
bg-light
@endsection
@section('content')
<div class="container">
    <post-comment :post="{{json_encode($post)}}"></post-comment>
</div>
@endsection
