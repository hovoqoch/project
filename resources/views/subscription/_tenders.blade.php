@if(count($tenders) > 0)
<table class="table">
    <thead>
        <tr>
            <th scope="col">Tender</th>
            <th scope="col">Reports</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tenders as $tender)
        <tr>
            <td>
                <a href="{{route('tender.show', $tender->id)}}">{{$tender->title}}</a>
            </td>
            <td>
                {{$tender->parcelsCount()}}
            </td>
            <td>
                @if($serializer->canDownloadTender($tender))
                    <a href="{{route('tender.show', $tender->id)}}" class="btn btn-dark">View</a>
                @else
                    <pay-with :title="'Subscribe'" :payment="{{json_encode([
                        'payload' => [
                            'tender' => $tender->id,
                            'action' => 'buyTender',
                            'points' => appPackages()['tender']['points'] 
                        ],
                        'paypal' => [
                            'description' => "Buy tender {$tender->title} of supplier {$tender->supplier->name}",
                            'amount' => [
                                'currency_code' => 'USD',
                                'value' => appPackages()['tender']['price']
                            ]
                        ]
                    ])}}">
                </pay-with>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="alert alert-info">
    You don't have ..
</div>
@endif


