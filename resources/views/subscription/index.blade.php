@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-primary">{{__("Subscription")}}</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
        <a class="nav-link active" id="subscription-tab" data-toggle="tab" href="#subscription" role="tab"
                aria-selected="true">Subscriptions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tender-tab" data-toggle="tab" href="#tender" role="tab"
                aria-selected="false">Tenders</a>
        </li>
    </ul>
    <div class="p-4 tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="subscription" role="tabpanel">
            @include('subscription/_all')
        </div>
        <div class="tab-pane fade" id="tender" role="tabpanel">
            @include('subscription/_tenders')
        </div>
    </div>
</div>
@endsection
