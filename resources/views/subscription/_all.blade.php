@if(count($subscriptions) > 0)
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Start At</th>
            <th scope="col">End At</th>
            <th scope="col">Remaining</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($subscriptions as $subscription)
        <tr>
            <td>{{$subscription->id}}</td>
            <td>{{$subscription->created_at->toDayDateTimeString()}}</td>
            <td>{{$subscription->ended_at->toDayDateTimeString()}}</td>
            <td>{{$subscription->remaining()}} Day/Days</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="alert alert-info">
    You don't have ..
</div>
@endif
