@component('mail::message')
# Hello {{$name}}

Unfortunately the subscription ended **{{$ended_at}}**

@component('mail::table')
|Type|Created At|Ended At|
| ------------- |:-------------:| --------:|
|{{$subscribe['type']}}|{{$subscribe['created_at']}}|{{$subscribe['ended_at']}}|
@endcomponent

@component('mail::button', ['url' => '/my/profile'])
View profile
@endcomponent

Thank you for using our application!<br>
{{ config('app.name') }}
@endcomponent
