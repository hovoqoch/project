<div class="d-none d-lg-block shadow-sm bg-white">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div>
                <ul class="nav flex-center">
                    <li class="nav-item">
                        <a class="navbar-brand mr-4" href="{{ url('/') }}">
                            <img src="/images/logo.png" alt="">
                        </a>
                    </li>
                @foreach (settings('menuLinks') as $link)
                    @if($link['dropdown'])
                        <li class="nav-item dropdown">
                            <a class="nav-link text-black dropdown-toggle fw-500 text-uppercase  {{$link['class']}}" href="#" id="navbar-{{$link['text']}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{$link['text']}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbar-{{$link['text']}}">
                                @foreach ($link['list'] as $list)
                                    <a class="dropdown-item {{$list['class']}}" href="{{$list['url']}}">{{$list['text']}}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                    <li class="nav-item">
                        <a 
                            class="nav-link text-black fw-500 text-uppercase {{$link['class']}}" 
                            href="{{$link['url']}}">{{$link['text']}}</a>
                    </li>
                    @endif
                @endforeach
                </ul>
            </div>
            <div class="flex-center">
                <ul class="nav">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item">
                        <a href="#" class="btn btn-outline-primary mr-3" data-toggle="modal"
                        data-target="#authModal">Log In</a>
                        <a href="#" class="btn btn-primary" data-toggle="modal"
                        data-target="#authModal" data-auth="Register">Sign Up</a>
                    </li>
                    @else
                        <li class="nav-item">
                            <a href="{{route('my.wishlist')}}" class="nav-link">
                                <img src="/images/wishlist-in.png" alt="">
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('my.bonus')}}" class="nav-link">
                                <span class="badge badge-light">{{Auth::user()->points}} points</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{Auth::user()->getAvatar()}}" alt="" class="img--xs rounded-circle">
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-black" href="{{ route('parcel.index') }}">
                                    {{ __('My Uploads') }}
                                </a>
                                <a class="dropdown-item text-black" href="{{ route('my.purchases') }}">
                                    {{ __('My Purchases') }}
                                </a>
                                <a class="dropdown-item text-black" href="{{ route('my.subscription') }}">
                                    {{ __('My Subscription') }}
                                </a>
                                <a class="dropdown-item text-black" href="{{ route('profile.edit') }}">
                                    {{ __('My Profile') }}
                                </a>
                                <a class="dropdown-item text-black" href="{{ route('my.viewed.parcels') }}">
                                    Viewed Parcels <span class="badge badge-primary">
                                        @if($cookies = Cookie::get('viewed_thread'))
                                            {{count(json_decode($cookies, true))}}
                                        @else
                                            0
                                        @endif
                                    </span>
                                </a>
                                @if(Auth::user()->isAdmin())
                                <a class="dropdown-item text-black" href="{{route('admin.tender.index')}}">
                                    {{__('Dashboard')}}
                                </a>
                                @endif
                                <a class="dropdown-item text-black" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('parcel.create')}}" class="btn btn-outline-primary">{{__('Upload')}}</a>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="d-block d-lg-none shadow-sm bg-white">
    <div class="container">
        <div class="text-center">
            <a class="navbar-brand mr-4" href="{{ url('/') }}">
                <img src="/images/logo.png" alt="">
            </a>
        </div>
        <div>
            <ul class="nav nav-fill">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a href="#" class="btn btn-outline-primary mr-3" data-toggle="modal"
                    data-target="#authModal">Log In</a>
                    <a href="#" class="btn btn-primary" data-toggle="modal"
                    data-target="#authModal" data-auth="Register">Sign Up</a>
                </li>
                @else
                    <li class="nav-item">
                        <a href="{{route('my.wishlist')}}" class="nav-link">
                            <img src="/images/wishlist-in.png" alt="">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('my.bonus')}}" class="nav-link">
                            <span class="badge badge-light">{{Auth::user()->points}} points</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="{{Auth::user()->getAvatar()}}" alt="" class="img--xs rounded-circle">
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item text-black" href="{{ route('parcel.index') }}">
                                {{ __('My Uploads') }}
                            </a>
                            <a class="dropdown-item text-black" href="{{ route('my.purchases') }}">
                                {{ __('My Purchases') }}
                            </a>
                            <a class="dropdown-item text-black" href="{{ route('my.subscription') }}">
                                {{ __('My Subscription') }}
                            </a>
                            <a class="dropdown-item text-black" href="{{ route('profile.edit') }}">
                                {{ __('My Profile') }}
                            </a>
                            <a class="dropdown-item text-black" href="{{ route('my.viewed.parcels') }}">
                                Viewed Parcels <span class="badge badge-primary">
                                    @if($cookies = Cookie::get('viewed_thread'))
                                        {{count(json_decode($cookies, true))}}
                                    @else
                                        0
                                    @endif
                                </span>
                            </a>
                            @if(Auth::user()->isAdmin())
                            <a class="dropdown-item text-black" href="{{route('admin.tender.index')}}">
                                {{__('Dashboard')}}
                            </a>
                            @endif
                            <a class="dropdown-item text-black" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
            @auth
            <a href="{{route('parcel.create')}}" class="btn btn-outline-primary btn-block">{{__('Upload')}}</a>
            @endauth
        </div>
        <div>
            <ul class="nav flex-center mt-4">
            @foreach (settings('menuLinks') as $link)
                @if($link['dropdown'])
                    <li class="nav-item dropdown">
                        <a class="nav-link text-black dropdown-toggle fw-500 text-uppercase  {{$link['class']}}" href="#" id="navbar-{{$link['text']}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$link['text']}}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbar-{{$link['text']}}">
                            @foreach ($link['list'] as $list)
                                <a class="dropdown-item {{$list['class']}}" href="{{$list['url']}}">{{$list['text']}}</a>
                            @endforeach
                        </div>
                    </li>
                @else
                <li class="nav-item">
                    <a 
                        class="nav-link text-black fw-500 text-uppercase {{$link['class']}}" 
                        href="{{$link['url']}}">{{$link['text']}}</a>
                </li>
                @endif
            @endforeach
            </ul>
        </div>
    </div>
</div>