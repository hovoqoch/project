<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="d-flex flex-column">
    @include('layouts.nav')
    <div id="app">
        <main class="mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/tenders*') ? 'active' : '' }}"
                                href="{{route('admin.tender.index')}}">Tender</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/suppliers*') ? 'active' : '' }}" href="{{route('admin.supplier.index')}}">Supplier</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}" href="{{route('admin.user.index')}}">User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/reports*') ? 'active' : '' }}" href="{{route('admin.report.index')}}">Report</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('admin/ads*') ? 'active' : '' }}" href="{{route('admin.ads.index')}}">Ads</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-10 border-left">
                        @yield('content')
                    </div>
                </div>
            </div>
            <toaster status="{{session('status')}}"></toaster>
        </main>
    </div>
    @include('layouts.footer')
</body>

</html>
