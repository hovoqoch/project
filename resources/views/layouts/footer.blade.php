    @yield('hurryup')
    <footer class="footer mt-auto py-3">
        <div class="container">
            <nav class="navbar navbar-expand p-4">
                <div class="container">
                    <ul class="navbar-nav">
                        @foreach (settings('footerLinks') as $link)
                            <li class="nav-item">
                                <a class="nav-link" href="{{$link['url']}}">{{$link['text']}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <ul class="navbar-nav">
                        @foreach (settings('socialLinks') as $link)
                            <li class="nav-item">
                                <a class="nav-link" href="{{$link['url']}}">
                                    {!! $link['icon'] !!}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
    </footer>