
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Uppy</title>
    <link href="https://transloadit.edgly.net/releases/uppy/v1.3.0/uppy.min.css" rel="stylesheet">
</head>

<body>
    <div style="display: flex; justify-content: space-around;">
        <div class="files"></div>
        <div class="images"></div>
    </div>

    <script src="https://transloadit.edgly.net/releases/uppy/v1.3.0/uppy.min.js"></script>
    <script>
        Uppy.Core()
            .use(Uppy.Dashboard, {
                inline: true,
                target: '.files',
                width: 400,
            })
            .use(Uppy.XHRUpload, {
                endpoint: '/api/upload'
            })
        Uppy.Core()
            .use(Uppy.Dashboard, {
                inline: true,
                target: '.images',
                width: 400,
            })
            .use(Uppy.XHRUpload, {
                endpoint: '/api/upload'
            })
    </script>
</body>

</html>