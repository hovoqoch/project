@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-primary">{{__("My Wishlist")}}</h3>
    <parcels :url="'{{route('my.wishlist')}}'"></parcels>
</div>
@endsection