@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-primary">{{__("My Notifications")}}</h3>
    
    @if(count($notifications) > 0)
    <ul class="list-group list-group-flush">
    @foreach ($notifications as $notification)
        <li class="list-group-item">
            {!! implode($notification->data,' ') !!}
        </li>
    @endforeach
    </ul>
    <form method="POST" action="">
        @csrf
        <button class="btn btn-info btn-block">Mark all as read</button>
    </form>
    @else
        <div class="alert alert-info">
            No notifications
        </div>
    @endif
</div>
@endsection