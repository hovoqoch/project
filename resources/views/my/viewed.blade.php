@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-primary">{{__("My Viewed")}}</h3>
    <parcels :url="'{{route('my.viewed.parcels')}}'"></parcels>
</div>
@endsection