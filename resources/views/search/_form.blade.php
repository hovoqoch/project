<section class="mb-2">
    <div class="search">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="search__intro">
                        <h3 class="text-black mb-3">
                            SafeTrade Reports
                        </h3>
                        <p class="text-black">
                            View multiple expert reports, save time and money. Buy with confidence.
                        </p>
                    </div>
                    <div class="search__form">
                        <form-search action="{{route('search')}}">
                        </form-search>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    