@component('components.filter', ['section' => 'size', 'title' => 'Size'])
<div class="form-row">
    <div class="col">
        <input type="number" name="size[from]" value="{{request("size.from") ?? ''}}" class="form-control" placeholder="From">
    </div>
    <div class="col">
        <input type="number" name="size[to]" value="{{request("size.to") ?? ''}}" class="form-control" placeholder="To"> 
    </div>
</div>    
@endcomponent