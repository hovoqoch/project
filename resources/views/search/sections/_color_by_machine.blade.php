@component('components.filter', ['section' => 'color_by_machine', 'title' => 'Color by machine'])
    <select name="color_by_machine" class="selectpicker form-control" id="">
        @foreach (settings('colorList') as $color)
        <option value="{{$color['id']}}" @if (request('color_by_machine')==$color['id'])
            selected
            @endif>{{$color['title']}}</option>
        @endforeach
    </select>
@endcomponent