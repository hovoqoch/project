<div class="filter mb-3">
    <button 
        class="show btn btn-sm collapse float-right" 
        data-toggle="collapse" 
        data-target="#section-status">
    </button>
    <p>Status</p>
    <div class="collapse show collapsed p-1" id="section-status">
        @foreach (settings('statusList') as $status)
            <div class="form-check">
            <input class="form-check-input" type="radio" name="status" id="status-{{$status['id']}}" value="{{$status['id']}}"
                @if(request('status') == $status['id']) checked @endif
                >
                <label class="form-check-label" for="status-{{$status['id']}}">
                    {{$status['title']}}
                </label>
            </div>
        @endforeach
    </div>
</div>