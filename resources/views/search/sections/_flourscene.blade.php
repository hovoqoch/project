@component('components.filter', ['section' => 'flourscene', 'title' => 'Flourscene'])
@foreach (settings('fluorScenceList') as $flourscene)
<div class="form-check">
    <input class="form-check-input" type="radio" name="flourscene" id="flourscene-{{$flourscene['id']}}" value="{{$flourscene['id']}}"
    @if(request('flourscene') == $flourscene['id']) checked @endif
    >
    <label class="form-check-label" for="flourscene-{{$flourscene['id']}}">
        {{$flourscene['title']}}
    </label>
</div>
@endforeach
@endcomponent