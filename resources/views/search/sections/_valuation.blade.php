@component('components.filter', ['section' => 'valuation', 'title' => 'Valuation'])
<div class="form-row">
    <div class="col">
        <input type="number" name="valuation_from" value="{{request("valuation_from") ?? ''}}" class="form-control" placeholder="From">
    </div>
    <div class="col">
        <input type="number" name="valuation_to" value="{{request("valuation._to") ?? ''}}" class="form-control" placeholder="To"> 
    </div>
</div>    
@endcomponent