<div class="filter mb-3">
    <button 
        class="show btn btn-sm collapse float-right" 
        data-toggle="collapse" 
        data-target="#section-tender">
    </button>
    <p>Tender</p>
    <div class="collapse show collapsed p-1" id="section-tender">
        <select name="tender" class="selectpicker form-control" id="">
            @foreach ($tenders as $tender)
            <option value="{{$tender->id}}" @if (request('tender')==$tender->id)
                selected
                @endif>{{$tender->title}}</option>
            @endforeach
        </select>
    </div>
</div>