@component('components.filter', ['section' => 'color_by_eye', 'title' => 'Color by eye'])
    <select name="color_by_eye" class="selectpicker form-control" id="">
        @foreach (settings('colorList') as $color)
        <option value="{{$color['id']}}" @if (request('color_by_eye')==$color['id'])
            selected
            @endif>{{$color['title']}}</option>
        @endforeach
    </select>
@endcomponent