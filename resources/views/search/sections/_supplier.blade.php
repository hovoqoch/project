<div class="filter mb-3">
    <button 
        class="show btn btn-sm collapse float-right" 
        data-toggle="collapse" 
        data-target="#section-supplier">
    </button>
    <p>Supplier</p>
    <div class="collapse show collapsed p-1" id="section-supplier">
        <select name="supplier" class="selectpicker form-control" id="">
            @foreach ($suppliers as $supplier)
            <option value="{{$supplier->id}}" @if (request('supplier')==$supplier->
                id)
                selected
                @endif>{{$supplier->name}}</option>
            @endforeach
        </select>
    </div>
</div>