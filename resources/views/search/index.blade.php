@extends('layouts.app')

@section('content')
<div class="container">
    <section class="mb-5">
        <search 
            :data="{{json_encode([
            'colors' => settings('colorList'),
            'flourscene' => settings('fluorScenceList'),
            'request' => $request
        ])}}"></search>
    </section>
</div>
@endsection

@section('hurryup')
@include('components.hurryup')
@endsection
