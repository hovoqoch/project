<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="paypal-button"></div> 
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script>
      paypal.Button.render({
        env: 'sandbox', // Or 'production'
        payment: function(data, actions) {
          return actions.request.post('/api/create-payment')
            .then(function(res) {
              return res.id;
            });
        },
        onAuthorize: function(data, actions) {
          console.log(data);
          return actions.request.post(`/api/execute-payment/${data.paymentID}/${data.payerID}`)
            .then(function(res) {
            });
        }
      }, '.paypal-button');
    </script>
</body>
</html>