@extends('layouts.app')

@section('content')
<div class="container">
    <div class="mb-4">
        <h3 class="text-primary">My Bonus</h3>
        <div class="alert alert-light">
            Get 50 points by upload report, or
            <pay-with :title="'By Now'" :payment="{{json_encode([
            'payload' => [
                'action' => 'buyPoints',
                'points' => appPackages()['points'][0]['points'] 
            ],
            'paypal' => [
                'description' => "Buy points",
                'amount' => [
                    'currency_code' => 'USD',
                    'value' => appPackages()['points'][0]['price']
                ]
            ]
        ])}}">
            </pay-with>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>Action</th>
                <th>Status</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($bonuses as $bonus)
            <tr>
                <td>{{$bonus->id}}</td>
                <td>{{$bonus->action['for']}}</td>
                <td>
                    @if($bonus->action['effect'] == 'ADD')
                    <span class="badge badge-success">+ {{$bonus->action['points']}}</span>
                    @else
                    <span class="badge badge-danger">- {{$bonus->action['points']}}</span>
                    @endif
                </td>
                <td>{{$bonus->created_at->toDateString()}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
