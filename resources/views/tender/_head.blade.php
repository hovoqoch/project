<section class="mb-5">
    <div class="row">
        <div class="col">
            @if($canDownloadTender)
            <div class="alert alert-info">You can download any parcels for the current tender <a
                    href="{{route('my.subscription', $tender->id)}}">See more</a></div>
            @else
            <div class="mb-2 float-right">
                <pay-with :title="'Subscribe for this tender'" :payment="{{json_encode([
                        'payload' => [
                            'tender' => $tender->id,
                            'action' => 'buyTender',
                            'points' => appPackages()['tender']['points'] 
                        ],
                        'paypal' => [
                            'description' => "Buy tender {$tender->title} of supplier {$tender->supplier->name}",
                            'amount' => [
                                'currency_code' => 'USD',
                                'value' => appPackages()['tender']['price']
                            ]
                        ]
                    ])}}">
                </pay-with>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-4 flex-center">
            <div>
                <h1 class="text-primary h2">{{$tender->title}}</h1>
                <h4 class="h5">{{$tender->supplier->name}}</h4>
                <p>
                    <span class="badge badge-success card__badge p-2 rounded-pill">{{$tender->formatStatus()}}</span>
                </p>
                <p class="w-12">
                    {{$tender->description}}
                </p>
                <p>
                    <strong>Start date:</strong> {{$tender->started_at->toFormattedDateString()}} <br>
                    <strong>End date:</strong> {{$tender->ended_at->toFormattedDateString()}}
                </p>
            </div>
        </div>
        <div class="col-8">
            @if($tender->hasImage())
            <img src="{{$tender->getImage()}}" alt="" class="img--s1">
            @else
            <div class="h-100 no-image w-100"></div>
            @endif
        </div>
    </div>
</section>
