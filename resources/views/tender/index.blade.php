@extends('layouts.app')

@section('content')
<div class="container">
    @component('components.page-logo')
    <h1 class="text-primary h2">{{__('Tender')}}</h1>
    <p class="text-muted">{{__('Choose among multiple different tenders')}}</p>
    @endcomponent
    @include ('tender/_active')
    @include ('tender/_closed')
    @include ('tender/_upcoming')
</div>
@endsection
