<section class="mb-4">
    <div class="row">
        <div class="col">
            <button class="show btn btn-sm collapse float-right" data-toggle="collapse"
                data-target="#section-closed">
            </button>
            <h4 class="border-bottom h4 mb-4 pb-3 text-primary">Closed Tender</h4>
        </div>
    </div>
    <div class="collapse show collapsed" id="section-closed">
        <our-tender :tenders="{{json_encode($closed)}}"></our-tender>
    </div>
</section>