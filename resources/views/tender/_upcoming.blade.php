<section class="mb-4">
    <div class="row">
        <div class="col">
            <button class="show btn btn-sm collapse float-right" data-toggle="collapse"
                data-target="#section-upcoming">
            </button>
            <h4 class="border-bottom h4 mb-4 pb-3 text-primary">Upcoming Tender</h4>
        </div>
    </div>
    <div class="collapse show collapsed" id="section-upcoming">
        <our-tender :tenders="{{json_encode($upcoming)}}"></our-tender>
    </div>
</section>