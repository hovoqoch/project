<a href="{{route('tender.show', $tender->id)}}">
    <div class="card">
    <span class="badge badge-success card__badge p-2 rounded-pill">{{$tender->formatStatus()}}</span>
        @if($tender->hasImage())
        <img src="{{$tender->getImageUrl()}}" class="card-img-top" alt="">
        @else
        <div class="card-img-top no-image"></div>
        @endif
        <div class="card__shadow p-2">
            <span>{{$tender->title}}</span>
            <span class="float-right">{{$tender->parcelsCount()}} {{__('Parcels')}}</span>
        </div>
    </div>
</a>