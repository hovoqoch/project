@extends('layouts.app')

@section('content')
<div class="container">
    @include('tender._head')
    <parcels :url="'{{route('tender.show', $tender->id)}}'"></parcels>
</div>
@endsection

@section('hurryup')
    @include('components.hurryup')
@endsection