<div class="collapse show collapsed" id="section-reports">
    <div class="row">
        @foreach ($reports as $index => $report)
        <div class="col-2">
            <p>Report ({{$index + 1}})</p>
            <a href="{{route('parcel.download', $report->id)}}">
                <img src="/images/file.png" class="img-fluid mb-2" alt="">
            </a>
            <div class="text-center">
                @if($SHOW_PARCEL_INFO)
                <vote 
                    type="'report'" 
                    init='{
                        "value" : {{Auth::user()->isVotedFor('report', $report->id)}},
                        "up": {{$report->votes('report')}},
                        "down": {{$report->votes('report', false)}}
                    }'
                    parcel="{{$report->id}}">
                </vote>
                @else
                    @include('components/_rates', ['parcel' => $report, 'type' => 'report'])
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
