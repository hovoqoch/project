<div class="collapse show collapsed" id="section-review">
    <div class="row">
        <div class="col-4">
            <h5>({{$avgRate['_avg']}})</h5>
            <ul class="list-unstyled">
                <li class="mb-4 rates__lg">{!! stars($avgRate['avg']) !!} ({{$avgRate['count']}})</li>
                @foreach ($parcel->rates as $rate)
                    <li class="mb-2">{!! stars($rate->rate) !!} <span class="ml-2">({{$rate->count}})</span></li> 
                @endforeach
            </ul>
        </div>
        <div class="col-8">
            @auth
            <a href="" class="btn btn-primary btn-sm mb-4" data-toggle="modal"
                data-target="#reviewModal">{{__("Add Your Review")}}</a>
                @include('modals._review')
            @endauth
            <ul class="list-unstyled">
                @foreach ($parcel->reviews as $review)
                <li class="media mb-4">
                    <img class="mr-3 img--sm" src="/images/avatar.png" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5>{{ $review->user->name }} {!! stars($review->rate) !!}</h5>
                        <p class="mt-0 mb-1">
                            {{$review->comment}}
                        </p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
