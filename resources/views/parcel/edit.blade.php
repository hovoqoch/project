@extends('layouts.app')

@section('content')
<div class="container mb-4">
    <div class="row">
        <div class="col">
            <h3 class="mb-3 mb-3 text-primary">({{$parcel->parcel_number}} /
                {{$parcel->tender->title}})</h3>
            @if(! $parcel->hasFile())
            <div class="alert alert-info">
                Parcel is draft you should upload sarin file to go live.
                <button id="uppy-file" {{$parcel->hasFile() ? 'disabled' : ''}} class="btn btn-info mr-2">Upload
                    Files</button>
                <button id="uppy-img" class="btn btn-info">Upload
                    Images</button>
            </div>
            @else
            <div class="alert alert-success">
                Parcel is live <strong>Approval</strong>. <button id="uppy-file" {{$parcel->hasFile() ? 'disabled' : ''}} class="btn btn-success mr-2">Upload
                    Files</button>
                <button id="uppy-img" class="btn btn-success">Upload
                    Images</button>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <form method="POST" action="{{ route('parcel.update', $parcel->id) }}" autocomplete="off" novalidate>
                @csrf
                <input type="hidden" name="_method" value="PUT">
                @include('parcel._form', ['parcel' => $parcel])
            </form>
        </div>
        <div class="col-lg-6">
            <uppy 
                trigger="#uppy-img" 
                :restrictions="{{json_encode([
                    "maxNumberOfFiles" => 4,
                    "minNumberOfFiles" => 1,
                    "maxFileSize" => 2097152,
                    "allowedFileTypes" => ["image/png", "image/jpg", "image/jpeg"]
                ])}}"
                title="Sarin Images *.png, *.jpg, *.jpeg maxsize 2M"
                url="{{route('parcel.media.upload', $parcel->id)}}">
            </uppy>
            <uppy 
                trigger="#uppy-file"
                :restrictions="{{json_encode([
                    "maxNumberOfFiles" => 1,
                    "minNumberOfFiles" => 1,
                    "maxFileSize" => 20971520,
                    "allowedFileTypes" => [".tnd", ".adv", ".cap"]
                ])}}"
                title="Sarin Files *.tnd, *.adv, *.cap maxsize 20M" 
                url="{{route('parcel.media.upload', $parcel->id)}}">
            </uppy>
            @include('parcel._media')
        </div>
    </div>
</div>
@endsection
