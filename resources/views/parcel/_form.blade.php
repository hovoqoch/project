<number-group label="{{__('Size')}}" id="size" value="{{old('size') ?? $parcel->size}}"
    errors="{{$errors->first('size')}}">
</number-group>
<select-group label="{{__('Color by eye')}}" id="color_by_eye" options="{{json_encode(settings('colorList'))}}"
    value="{{old('color_by_eye') ?? $parcel->color_by_eye}}" errors="{{$errors->first('color_by_eye')}}">
</select-group>
<text-area-group label="{{__('Comment on color by eye')}}" id="comment_color_by_eye"
    value="{{old('comment_color_by_eye') ?? $parcel->comment_color_by_eye}}"
    errors="{{$errors->first('comment_color_by_eye')}}">
</text-area-group>
<select-group label="{{__('Color by machine')}}" id="color_by_machine"
    options="{{json_encode(settings('colorList'))}}"
    value="{{old('color_by_machine') ?? $parcel->color_by_machine}}"
    errors="{{$errors->first('color_by_machine')}}">
</select-group>
<select-group label="{{__('Fluorescence')}}" id="flourscene" options="{{json_encode(settings('fluorScenceList'))}}"
    value="{{old('flourscene') ?? $parcel->flourscene}}" errors="{{$errors->first('flourscene')}}">
</select-group>
<text-area-group label="{{__('Comment on fluorescence')}}" id="comment_flourscene"
    value="{{old('comment_flourscene') ?? $parcel->comment_flourscene}}"
    errors="{{$errors->first('comment_flourscene')}}">
</text-area-group>

<div class="row">
    <div class="col">
        <number-group label="{{__('Valuation from')}}" id="valuation_from" value="{{old('valuation_from') ?? $parcel->valuation_from}}"
            errors="{{$errors->first('valuation_from')}}">
        </number-group>
    </div>
    <div class="col">
        <number-group label="{{__('Valuation to')}}" id="valuation_to" value="{{old('valuation_to') ?? $parcel->valuation_to}}"
            errors="{{$errors->first('valuation_to')}}">
        </number-group>
    </div>
</div>
<div class="row">
    <div class="col">
        <button class="btn btn-primary btn-block">{{__('Save')}}</button>
    </div>
    <div class="col">
        <button class="btn btn-outline-danger btn-block" type="button">{{__('Cancel')}}</button>
    </div>
</div>
