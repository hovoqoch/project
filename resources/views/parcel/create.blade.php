@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <form-upload :list="{{json_encode([
                'tender' => $tenders,
                'color' => settings('colorList'),
                'fluorescence' => settings('fluorScenceList')
            ])}}"></form-upload>
        </div>
    </div>
</div>
@endsection
