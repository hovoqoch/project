@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{route('parcel.create')}}" class="btn btn-primary float-right">{{__('Upload new parcel')}}</a>
    <h3 class="mb-4 text-primary">{{__('My Uploads')}}</h3>
    <div class="btn-group mb-3" role="group">
        <a class="btn {{$type == 'live' ? 'btn-light' : ''}} btn-sm" href="{{route('parcel.index', ['type' => 'live'])}}">Live</a>
        <a class="btn {{$type == 'draft' ? 'btn-light' : ''}}  btn-sm" href="{{route('parcel.index', ['type' => 'draft'])}}">Draft</a>
    </div>
    <parcels 
        :url="'{{route('parcel.index', ['type' => $type])}}'"
        :like="true"
        :action="true"
        :token="'{{csrf_token()}}'"
    ></parcels>
</div>
@endsection
