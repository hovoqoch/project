<div class="collapse show collapsed" id="section-about">
    <div class="row">
        <div class="col font-weight-bold">
            <p>#</p>
            <p>Tender</p>
            <p>Parcel Number</p>
            <p>Size</p>
            <p>Color by eye</p>
            <p>Comment on color by eye</p>
            <p>Valuation</p>
            <p>Color by machine</p>
            <p>Flourscene</p>
            <p>Comment on flourscene</p>
            <p>Rates</p>
        </div>
        @foreach ($reports as $index=>$report)
        <div class="col">
            <p>Report ({{$index + 1}})</p>
            <p>{{$report->tender->title}}</p>
            <p>{{$report->parcel_number}}</p>
            <p>@if($SHOW_PARCEL_INFO){{$report->size}}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){{$report->color_by_eye}}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){!!$report->presentComment('comment_color_by_eye')!!}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){{$report->valuation_from}} - {{$report->valuation_to}}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){{$report->color_by_machine}}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){{$report->flourscene}}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO){!!$report->presentComment('comment_flourscene')!!}@else . @endif</p>
            <p>@if($SHOW_PARCEL_INFO)
                <vote 
                    type="'content'" 
                    init='{
                        "value" : {{Auth::user()->isVotedFor('content', $report->id)}},
                        "up": {{$report->votes('content')}},
                        "down": {{$report->votes('content', false)}}
                    }'
                    parcel="{{$report->id}}">
                </vote>
                @else 
                    @include('components/_rates', ['parcel' => $parcel, 'type' => 'content'])
                @endif
            </p>
        </div>
        @endforeach
    </div>
</div>