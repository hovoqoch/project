@extends('layouts.app')

@section('content')
<div class="container">
    <section class="parcel__section parcel__intro">
        <div>
            @if($meta['showPrice'])
            <span class="font-weight-bolder text-danger float-right">${{appPackages()['thread']['price']}}</span>
            @endif
            <h4 class="text-primary">
                Parcel {{$reports->first()->parcel_number}}
                <small
                    class="text-muted">({{ trans_choice('text.reports', count($reports), ['count' => count($reports)]) }})</small>
            </h4>
        </div>
        <div>
            <div class="float-right">
                @if($meta['boughtIt'])
                <a class="border btn btn__prime mt-1 rounded"
                    href="{{route('parcel.download_all', $parcelId)}}">Download</a>
                @else
                @if($meta['user'])
                <wishlist :thread="'{{$thread}}'" :in="{{ $meta['inWishList'] ? 'true' : 'false'}}">
                </wishlist>
                <pay-with :title="'Buy All Reports'" :payment="{{json_encode([
                        'payload' => [
                            'thread' => $thread,
                            'action' => 'buyThread'
                        ],
                        'paypal' => [
                            'description' => "Buy all reports for parcel # $thread",
                            'amount' => [
                                'currency_code' => 'USD',
                                'value' => appPackages()['thread']['price']
                            ]
                        ]
                    ])}}">
                </pay-with>
                @endif
                @endif
            </div>
            {!! stars($rates['avg']) !!} ({{$rates['count']}})
        </div>
        <div class="mt-4">
            @if(count($images) > 0)
            <slick ref="slick" :options="{{json_encode([
                "infinite" => true,
                "slidesToShow" => 1,
                "slidesToScroll" => 1
            ])}}">
                @foreach ($images as $img)
                <div class="p-2">
                    @if($serializer->hasIt($meta['user'], $img->model)) <i class="fa fa-bookmark text-success"
                        title="This image uploaded by you"></i> @endif
                    <img src="{{$img->getUrl()}}" alt="" class="mt-3 w-100 parcel__image--fit">
                    <div class="text-center">
                        @if(!$meta['user'] || $serializer->hasIt($meta['user'], $img->model))
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-up"></i>
                            {{$img->model->votes('image', 1)}}
                        </button>
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-down"></i>
                            {{$img->model->votes('image', -1)}}
                        </button>
                        @endif
                        @if($meta['boughtIt'] && !$serializer->hasIt($meta['user'], $img->model))
                        <vote type="'image'" :init="{{json_encode([
                                "value" =>  $meta['user']->isVotedFor('image', $img->model->id),
                                "up" => $img->model->votes('image', 1),
                                "down" => $img->model->votes('image', -1)
                            ])}}" parcel="{{$img->model->id}}">
                        </vote>
                        @endif
                    </div>
                </div>
                @endforeach
            </slick>
            @else
            <div class="no-image h__x"></div>
            @endif
        </div>
    </section>
    <section class="parcel__section parcel__about">
        <div class="mb-4">
            <button class="show btn btn-sm collapse float-right" data-toggle="collapse" data-target="#section-about">
            </button>
            <h3 class="h5">About</h3>
        </div>
        <div class="collapse show collapsed" id="section-about">
            <div class="row">
                <div class="col-3 font-weight-bold">
                    <p>#</p>
                    <p>Tender</p>
                    <p>Parcel Number</p>
                    <p>Size</p>
                    <p>Color by eye</p>
                    <p>Comment on color by eye</p>
                    <p>Valuation from</p>
                    <p>Valuation to</p>
                    <p>Color by machine</p>
                    <p>Fluorescence</p>
                    <p>Comment on fluorescence</p>
                    <p>Rates</p>
                </div>
                @foreach ($reports as $index=>$report)
                <div class="col">
                    <p>@if($serializer->hasIt($meta['user'], $report))<i class="fa fa-bookmark text-success"
                            title="This report uploaded by you"></i> @endif Report ({{$index + 1}})</p>
                    <p>{{$report->tender->title}}</p>
                    <p>{{$report->parcel_number}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'size', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'color_by_eye', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'comment_color_by_eye', $meta['boughtIt'])}}&nbsp;
                    </p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'valuation_from', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'valuation_to', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'valuation_to', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'flourscene', $meta['boughtIt'])}}</p>
                    <p>{{$serializer->canSeeIt($report, $meta['user'], 'comment_flourscene', $meta['boughtIt'])}}&nbsp;
                    </p>
                    <p>
                        @if($meta['boughtIt'] && !$serializer->hasIt($meta['user'], $report))
                        <vote type="'content'" :init="{{json_encode([
                                "value" =>  $meta['user']->isVotedFor('content', $report->id),
                                "up" => $report->votes('content', 1),
                                "down" => $report->votes('content', -1)
                            ])}}" parcel="{{$report->id}}">
                        </vote>
                        @else
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-up"></i>
                            {{$report->votes('content', 1)}}
                        </button>
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-down"></i>
                            {{$report->votes('content', -1)}}
                        </button>
                        @endif
                    </p>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="parcel__section parcel__report">
        <div class="mb-4">
            <button class="show btn btn-sm collapse float-right" data-toggle="collapse" data-target="#section-reports">
            </button>
            <h3 class="h5">Reports</h3>
        </div>
        <div class="collapse show collapsed" id="section-reports">
            <div class="row">
                @foreach ($reports as $index => $report)
                <div class="col-2">
                    <p>@if($serializer->hasIt($meta['user'], $report)) <i class="fa fa-bookmark text-success"
                            title="This report uploaded by you"></i> @endif Report ({{$index + 1}})</p>
                    <a href="{{route('parcel.download', $report->id)}}">
                        <img src="/images/file.png" class="img-fluid mb-2" alt="">
                    </a>
                    <div class="text-center @if(Arr::has($downloaded, $report->id)) is-download @endif">
                        @if($meta['boughtIt'] && !$serializer->hasIt($meta['user'], $report))
                        <vote type="'report'" :init="{{json_encode([
                                "value" =>  $meta['user']->isVotedFor('report', $report->id),
                                "up" => $report->votes('report', 1),
                                "down" => $report->votes('report', -1)
                            ])}}" parcel="{{$report->id}}">
                        </vote>
                        @else
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-up"></i>
                            {{$report->votes('content', 1)}}
                        </button>
                        <button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-down"></i>
                            {{$report->votes('content', -1)}}
                        </button>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="parcel__section parcel__review">
        <div class="mb-4">
            <button class="show btn btn-sm collapse float-right" data-toggle="collapse" data-target="#section-review">
            </button>
            <h3 class="h5">Reviews</h3>
        </div>
        <div class="collapse show collapsed" id="section-review">
            <div class="row">
                <div class="col-4">
                    <h5>({{number_format($rates['avg'], 2)}})</h5>
                    <ul class="list-unstyled">
                        <li class="mb-4 rates__lg">{!! stars($rates['avg']) !!} ({{$rates['count']}})</li>
                    </ul>
                </div>
                <div class="col-8">
                    @auth
                    <a 
                        data-method="POST" 
                        data-action="{{ route('review.store') }}"
                        href="" 
                        class="btn btn-primary btn-sm mb-4" 
                        data-toggle="modal"
                        data-target="#reviewModal">
                        {{__("Add Your Review")}}
                    </a>
                    @include('modals._review')
                    @endauth
                    <ul class="list-unstyled">
                        @foreach ($reviews as $review)
                        <li class="media mb-4">
                            <img class="mr-3 img--sm" src="/images/avatar.png" alt="Generic placeholder image">
                            <div class="media-body">
                                <h5>{{ $review->user->name }} {!! stars($review->rate) !!}</h5>
                                <p class="mt-0 mb-1">
                                    {{$review->comment}}
                                </p>
                                @auth
                                <p>
                                    @if($review->user_id = Auth::user()->id)
                                    <a href="#" data-url="{{route('review.delete', $review->id)}}" data-toggle="modal" data-target="#deleteModal">Delete</a>
                                    |
                                    <a 
                                        data-method="PUT" 
                                        data-action="{{ route('review.update', $review->id) }}"
                                        data-comment="{{$review->comment}}"
                                        href="" 
                                        data-toggle="modal"
                                        data-target="#reviewModal">Edit</a>
                                    @endif
                                </p>
                                @endauth
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </section>
</div>
@include('modals/_delete')
@endsection
