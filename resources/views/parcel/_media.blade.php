<div class="row">
    @foreach ($parcel->media as $media)
    <div class="col-4">
        @if ($media->collection_name == 'images')
            <delete-media src="{{$media->getUrl('thumb')}}" parcel="{{$parcel->id}}" media="{{$media->id}}"></delete-media>
        @else
            <delete-media src="/images/file.png" parcel="{{$parcel->id}}" media="{{$media->id}}"></delete-media>
        @endif
    </div>
    @endforeach
</div>