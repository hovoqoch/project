@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-3 mb-3 text-primary">{{__('Upload Media')}} ({{$parcel->parcel_number}})</h3>
    <p>
        <small>Max Image (1), Max Report (1)</small>
        <div class="btn-group" role="group">
            <button id="uppy-img" {{$parcel->hasImage() ? 'disabled' : ''}} class="btn btn-info">Upload Images</button>
            <button id="uppy-file" {{$parcel->hasFile() ? 'disabled' : ''}} class="btn btn-info">Upload Files</button>
        </div>
        <uppy
            trigger="#uppy-img"
            :size="2097152"
            :types="{{json_encode(["image/png", "image/jpg", "image/jpeg"])}}"
            title="Sarin Images *.png, *.jpg, *.jpeg maxsize 2M"
            url="{{route('parcel.media.upload', $parcel->id)}}">
        </uppy>
        <uppy
            :types="{{json_encode([".tnd", ".adb", ".cap"])}}"
            :size="20971520"
            trigger="#uppy-file"
            title="Sarin Files *.tnd, *.adb, *.cap maxsize 20M"
            url="{{route('parcel.media.upload', $parcel->id)}}">
        </uppy>
    </p>
    @include('parcel._media')
    <p>
        <a href="{{ route('parcel.edit', $parcel->id) }}" class="btn">Edit Parcel</a>
    |
        <a href="{{ route('parcel.index') }}" class="btn">My Uploads</a>
    </p>
</div>
@endsection
