@extends('layouts.app')

@section('content')
    <form-group action="{{ route('register') }}" title="{{__('Sign Up')}}">
        @csrf
        @component('components.auth')@endcomponent
        <text-group
            id="name"
            label="{{__('Fullname')}}"
            value="{{old('name')}}"
            errors="{{$errors->first('name')}}">
        </text-group>
        <email-group
            id="email"
            label="{{__('Email')}}"
            value="{{old('email')}}"
            errors="{{$errors->first('email')}}">
        </email-group>
        <password-group
            id="password"
            label="{{__('Password')}}"
            errors="{{$errors->first('password')}}">
        </password-group>
        <text-group
            id="phone"
            label="{{__('Phone number')}}"
            value="{{old('phone')}}"
            errors="{{$errors->first('phone')}}">
        </text-group>
        <checkbox-group
            errors="{{$errors->first('receive_notification')}}"
            id="receive_notification"
            value="1"
            @if(old('receive_notification')) :checked="true" @endif
            :required="false"
            label="{{__('I would like to receive email notifications about offers.')}}">
        </checkbox-group>
        <checkbox-group
            errors="{{$errors->first('accept_policy')}}"
            id="accept_policy"
            value="1"
            @if(old('accept_policy')) :checked="true" @endif
            label="{!! __("I have read and accept the <a href=''>cookie policy</a>.") !!}">
        </checkbox-group>
        <submit-button title="{{__('Register')}}"></submit-button>
        <div class="text-center">
            <a class="btn btn-link btn-block" href="{{ route('login') }}">
                {{ __('Already have an account?') }} 
            </a>
        </div>
    </form-group>
@endsection
