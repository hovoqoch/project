<form action="{{ route('profile.update') }}" autocomplete="off" enctype="multipart/form-data"
    method="POST">
    @csrf
    <text-group value="{{old('name') ?? $user->name}}" label="{{__('Fullname')}}"
        errors="{{$errors->first('name')}}" id="name">
    </text-group>
    <number-group value="{{old('phone') ?? $user->phone}}" label="{{__('Phone')}}"
        errors="{{$errors->first('phone')}}" id="phone">
    </number-group>
    <email-group value="{{old('email') ?? $user->email}}" label="{{__('Email')}}"
        errors="{{$errors->first('email')}}" id="email">
    </email-group>
    <password-confirmation-group :required="false" id="password" label="{{__('New Password')}}"
        errors="{{$errors->first('password')}}">
    </password-confirmation-group>
    <div>
        <img src="{{Auth::user()->getAvatar()}}" alt="" class="img--sm img-thumbnail">
    </div>
    <file-group id="photo" label="Photo" :required="false" errors="{{$errors->first('photo')}}">
    </file-group>
    <submit-button title="{{__('Submit')}}"></submit-button>
</form>