@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-primary">{{__("My Profile")}}</h3>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#profile" role="tab" aria-selected="true">Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#rate" role="tab" aria-selected="false">Rate</a>
        </li>
    </ul>
    <div class="p-4 tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="profile" role="tabpanel">
            <div class="row">
                <div class="col-lg-8">
                    @include('auth/my_profile')
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="settings" role="tabpanel">
            <p>
                <settings></settings>
            </p>
            <p>
                <push-notification></push-notification>
            </p>
        </div>
        <div class="tab-pane fade" id="rate" role="tabpanel">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="badge badge-success badge-pill">Top 10</span>
                    <like :like="{{json_encode($reports['like'])}}" :dislike="{{json_encode($reports['dislike'])}}"></like>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Total reports
                    <span class="badge badge-info badge-pill">{{$reports['total']}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Total live reports
                    <span class="badge badge-success badge-pill">{{$reports['live']}}</span>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
