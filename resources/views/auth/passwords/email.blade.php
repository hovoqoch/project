@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-lg-6 offset-lg-3">
        <form action="{{ route('password.email') }}" method="post">
            @csrf
            <email-group
                label="{{__('Email')}}"
                id="email"
                value="{{old('email')}}"
                errors="{{$errors->first('email')}}">
            </email-group>
            <submit-button title="{{__('Send Password Reset Link')}}"></submit-button>    
        </form>
    </div>

</div>
@endsection
