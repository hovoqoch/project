@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-lg-6 offset-lg-3">
        <form action="{{ route('password.update') }}" method="post">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <email-group label="{{__('Email')}}" id="email" value="{{old('email')}}"
                errors="{{$errors->first('email')}}">
            </email-group>
            <password-group label="{{__('Password')}}" id="password" errors="{{$errors->first('password')}}">
            </password-group>
            <password-group label="{{__('Password Confirm')}}" id="password_confirmation" errors="">
            </password-group>
            <submit-button title="{{__('Reset Password')}}"></submit-button>
        </form>
    </div>
</div>
@endsection
