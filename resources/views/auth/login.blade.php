@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-4">
        <div class="col">
            <h1 class="text-primary h2">SafeTrade Reports</h1>
            <p class="text-muted">View multiple expert reports, save time and money. Buy with confidence.</p>
            <a href="#" class="btn btn-outline-primary" data-toggle="modal"
                        data-target="#authModal">LogIn / SignUp</a>
        </div>
    </div>
</div>
@endsection
