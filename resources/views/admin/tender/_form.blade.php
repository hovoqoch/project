<div class="form-group">
    <h3 class="text-primary">Add/Edit</h3>
</div>
@csrf
<text-group label="{{__('Mine')}}" id="title" value="{{old('title') ?? $tender->title}}"
    errors="{{$errors->first('title')}}">
</text-group>
<select-group label="{{__('Supplier')}}" id="supplier_id" options="{{json_encode($suppliers)}}"
    value="{{old('supplier_id') ?? $tender->supplier_id}}" errors="{{$errors->first('supplier_id')}}">
</select-group>
<div class="row">
    <div class="col">
        <date-group label="{{__('Start date')}}" id="started_at" value="{{old('started_at') ?? $tender->formatDate('started_at')}}"
            errors="{{$errors->first('started_at')}}">
        </date-group>
    </div>
    <div class="col">
        <date-group label="{{__('End date')}}" id="ended_at" value="{{old('ended_at') ?? $tender->formatDate('ended_at')}}"
            errors="{{$errors->first('ended_at')}}">
        </date-group>
    </div>
</div>
<div class="row">
    <div class="col">
        <date-group label="{{__('Bid start date')}}" id="bid_started_at" value="{{old('bid_started_at') ?? $tender->formatDate('bid_started_at')}}"
            errors="{{$errors->first('bid_started_at')}}">
        </date-group>
    </div>
    <div class="col">
        <date-group label="{{__('Bid end date')}}" id="bid_ended_at" value="{{old('bid_ended_at') ?? $tender->formatDate('bid_ended_at')}}"
            errors="{{$errors->first('bid_ended_at')}}">
        </date-group>
    </div>
</div>
<country-list 
    :selected="'{{old('country') ?? $tender->country}}'" 
    :errors="'{{$errors->first('country')}}'">
</country-list>
<text-area-group label="{{__('Description')}}" id="description" value="{{old('description') ?? $tender->description}}"
    errors="{{$errors->first('description')}}">
</text-area-group>

<div class="form-group">
    @if($tender->hasImage())
        <img src="{{$tender->getImageUrl()}}" alt="" class="">
    @endif
</div>

<div class="form-group">
    <label for="">Upload One Image</label>
    <input type="file" class="form-control" id="image" name="image" accept="image/*">
</div>

<button class="btn btn-primary btn-block">{{__('Submit')}}</button>
