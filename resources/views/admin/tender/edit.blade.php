@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <form action="{{route('admin.tender.update', $tender->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @include('admin.tender._form')
                <input type="hidden" name="_method" value="PUT">
            </form>
        </div>
    </div>
</div>
@endsection
