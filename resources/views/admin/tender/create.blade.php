@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <form action="{{route('admin.tender.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @include('admin.tender._form')
            </form>
        </div>
    </div>
</div>
@endsection
