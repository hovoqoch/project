@extends('layouts.admin')

@section('content')
<div class="mb-3">
    <a href="{{route('admin.tender.create')}}" class="btn btn-primary float-right">Add Tender</a>
    <h3 class="text-primary">Tender List</h3>
</div>
<table class="table">
    <thead>
        <tr>
            <th>id</th>
            <th>mine</th>
            <th>supplier</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Status</th>
            <th>action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tenders as $tender)
        <tr>
            <td>{{$tender->id}}</td>
            <td>{{$tender->title}}</td>
            <td>{{$tender->supplier->name}}</td>
            <td>
                {{$tender->started_at->toDateString()}}
            </td>
            <td>
                {{$tender->ended_at->toDateString()}}
            </td>
            <td>
                {{ $tender->formatStatus() }}
            </td>
            <td>
                <a href="{{route('admin.tender.edit', $tender->id)}}" class="btn btn-sm btn-info">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
