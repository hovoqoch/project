@extends('layouts.admin')

@section('content')
<div class="mb-3">
    <h3 class="text-primary">Reports List</h3>
</div>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>By</th>
            <th>Tender</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($reports as $report)
        <tr>
            <td>
                <a href="{{route('parcel.show', [$report->id, $report->thread])}}">
                    {{$report->id}}
                </a>
            </td>
            <td>{{$report->user->name}}</td>
            <td>
                <a  
                    target="_blank" 
                    href="{{route('tender.show', $report->tender->id)}}">{{$report->tender->title}}</a>
            </td>
            <td>
                @if($report->hasFile())
                    <i class="fas fa-paperclip text-success" title="Live"></i>                
                @else
                    <i class="fas fa-paperclip text-danger" title="Draft"></i>       
                @endif
            </td>
            <td>
                <button data-url="{{route('admin.report.delete', $report->id)}}" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    Delete
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $reports->links() }}

@include('modals/_delete')
@endsection
