@extends('layouts.admin')

@section('content')
<div class="mb-3">
    <h3 class="text-primary">Ads List</h3>
</div>
<table class="table">
    <thead>
        <tr>
            <th>Page</th>
            <th>Type</th>
            <th>Body</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ads as $ad)
        <tr>
            <td>{{$ad->page}}</td>
            <td>{{$ad->type}}</td>
            <td>{{$ad->body}}</td>
            <td>{{$ad->active ? 'active' : 'unactive'}}</td>
            <td>
                <a href="{{route('admin.ads.edit', $ad->id)}}" class="btn btn-sm btn-info">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
