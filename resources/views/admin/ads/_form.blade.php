<div class="form-group">
    <h3 class="text-primary">Add/Edit</h3>
</div>
@csrf
<select-group 
    label="{{__('Page')}}" 
    id="page" 
    options="{{json_encode([
        ['id' => '/', 'title' => 'Home']
    ])}}"
    value="{{old('page') ?? $ads->page}}" 
    errors="{{$errors->first('page')}}">
</select-group>
<select-group 
    label="{{__('Type')}}" 
    id="type" 
    options="{{json_encode([
        ['id' => 'welcome', 'title' => 'Welcome']
    ])}}"
    value="{{old('type') ?? $ads->type}}" 
    errors="{{$errors->first('type')}}">
</select-group>
<text-area-group label="{{__('Body')}}" id="body" value="{{old('body') ?? $ads->body}}"
    errors="{{$errors->first('body')}}">
</text-area-group>
<checkbox-group
    errors="{{$errors->first('active')}}"
    id="active"
    value="1"
    @if(old('active') || $ads->active) :checked="true" @endif
    :required="false"
    label="{{__('Active.')}}">
</checkbox-group>
<button class="btn btn-primary btn-block">{{__('Submit')}}</button>
