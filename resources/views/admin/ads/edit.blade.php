@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <form class="mb-4" action="{{route('admin.ads.update', $ads->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @include('admin.ads._form')
                <input type="hidden" name="_method" value="PUT">
            </form>
        </div>
    </div>
</div>
@endsection
