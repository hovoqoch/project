@extends('layouts.admin')

@section('content')
<div class="mb-3">
    <h3 class="text-primary">User List</h3>
</div>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Registration Date</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>
                {{$user->created_at->toDateString()}}
            </td>
            <td>
                <i 
                    data-toggle="tooltip" 
                    title=" @if($user->is_online) Online @else Offline @endif"
                    class="fas fa-signal @if($user->is_online) text-success @else text-danger @endif"></i>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $users->links() }}
@endsection
