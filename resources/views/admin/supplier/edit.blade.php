@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <form action="{{route('admin.supplier.update', $supplier->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @include('admin.supplier._form')
                <input type="hidden" name="_method" value="PUT">
            </form>
        </div>
    </div>
</div>
@endsection
