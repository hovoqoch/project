@extends('layouts.admin')

@section('content')
<div class="mb-3">
    <a href="{{route('admin.supplier.create')}}" class="btn btn-primary float-right">Add Supplier</a>
    <h3 class="text-primary">Supplier List</h3>
</div>
<table class="table">
    <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>country</th>
            <th>action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($suppliers as $supplier)
        <tr>
            <td>{{$supplier->id}}</td>
            <td>{{$supplier->name}}</td>
            <td>{{$supplier->country}}</td>
            <td>
                <a href="{{route('admin.supplier.edit', $supplier->id)}}" class="btn btn-sm btn-info">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
