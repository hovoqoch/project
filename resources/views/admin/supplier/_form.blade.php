<div class="form-group">
    <h3 class="text-primary">Add/Edit</h3>
</div>
@csrf
<text-group label="{{__('Name')}}" id="name" value="{{old('name') ?? $supplier->name}}"
    errors="{{$errors->first('name')}}">
</text-group>
<country-list 
    :selected="'{{old('country') ?? $supplier->country}}'" 
    :errors="'{{$errors->first('country')}}'">
</country-list>

<div class="form-group">
    @if($supplier->hasImage())
        <img src="{{$supplier->getImageUrl()}}" alt="" class="">
    @endif
</div>
<div class="form-group">
    <label for="">Upload One Image</label>
    <input type="file" class="form-control" id="image" name="image" accept="image/*">
</div>
<button class="btn btn-primary btn-block">{{__('Submit')}}</button>
