@php($SHOW_PARCEL_INFO = false)

@auth
@if(
Arr::has(Auth::user()->buy(), $parcel->thread) ||
Auth::user()->hasActiveSubscribe()
)
@php($SHOW_PARCEL_INFO = true)
@else
@php($SHOW_PARCEL_INFO = false)
@endif
@endauth
<div class="mb-3 parcel">
    <div class="row">
        <div class="col-lg-3">
            @if($parcel->hasImage())
            <img src="{{$parcel->getImageUrl()}}" alt="" class="parcel__image w-100">
            @else
            <div class="no-image parcel__image"></div>
            @endif
        </div>
        <div class="col-lg-9">
            <div class="parcel__details">
                <div class="parcel__action float-right">
                    <div class="font-weight-bolder parcel__price text-primary">${{prices()['parcel']}}</div>
                    {{ ($actions) ?? ''}}
                </div>
                <h4>
                    <a href="{{route('parcel.show', $parcel->id)}}">{{$parcel->parcel_number}}</a>
                    <small>
                        ({{$parcel->countAll($parcel->tender_id, $parcel->parcel_number)}})
                    </small>
                </h4>
                <p>
                    {!! stars($parcel->avgRate()['avg']) !!}
                    <span>({{$parcel->avgRate()['count']}})</span>
                </p>
                <div>
                    <ul class="list-unstyled">
                        <li>
                            <span class="title font-weight-bold">Color by eye</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->color_by_eye}}@else . @endif</span>
                            <span class="title font-weight-bold">Color by machine</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->color_by_machine}}@else . @endif</span>
                        </li>
                        <li>
                            <span class="title font-weight-bold">Size</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->size}}@else . @endif</span>
                            <span class="title font-weight-bold">Flourscene</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->flourscene}}@else . @endif</span>
                        </li>
                        <li>
                            <span class="title font-weight-bold">Valuation from</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->valuation_from}}@else . @endif</span>
                            <span class="title font-weight-bold">Valuation to</span>
                            <span class="value">@if($SHOW_PARCEL_INFO){{$parcel->valuation_to}}@else . @endif</span>
                        </li>
                    </ul>
                </div>
                {{ $slot }}
            </div>
        </div>
    </div>
</div>
