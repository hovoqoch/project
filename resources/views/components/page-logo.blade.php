<div class="mb-5 row">
    <div class="col-8 flex-center">
        <div>
            {{$slot}}
        </div>
    </div>
    <div class="col-4">
        <img src="/images/logo-header.png" class="img-fluid" alt="">
    </div>
</div>