<a href="{{route('parcel.show', $parcel->id)}}">
    @if($parcel->hasImage())
    <img 
        src="{{$parcel->getImageUrl()}}" 
        alt="" 
        class="card-img-top"
    >
    @else
    <div 
        class="card-img-top no-image"
    ></div>
    @endif
</a>
<div class="parcel__footer">
    <div class="div">
        <div class="float-right">
            {!! stars($parcel->avgRate()['avg']) !!}
            <span>({{$parcel->avgRate()['count']}})</span>
        </div>
        <h4 class="w-25">
            {{str_limit($parcel->parcel_number, 6, '...')}}
        </h4>
        <h4>
            {{$parcel->tender->title}}
        </h4>
    </div>
</div>
