@guest
<div class="h5 hurryup pb-4 pt-4">
    <div class="container hurryup__content">
        <div>
            Save money and get access to all reports for any tender in the world.
        </div>
        <div>
            <a href="#" 
            class="btn btn-primary" 
            data-toggle="modal"
            data-auth="Register"
            data-target="#authModal">Subscribe now</a>
        </div>
    </div>
</div>
@endguest