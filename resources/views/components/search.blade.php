<div class="col-3 bg-light">
    <form action="{{route('tender.filter', $tender->id)}}" method="GET">
        <h4>Status</h4>
        @foreach ($listItem as $item)
            <label for="status-{{$item->id}}">
                <input type="radio" name="status" id="status-{{$item->id}}"> {{$item->status}}
            </label>
        @endforeach
        <h4>Supplier</h4>
        <select name="supplier" id="">
            @foreach ($listItem as $item)
                <option value="{{$item->supplier}}">{{$item->supplier}}</option>
            @endforeach
        </select>
        <h4>Num of parcels</h4>
            <input type="number" name="parcel_count">
        <h4>Weight</h4>
        <input type="number" name="weight[]" id="">
        <input type="number" name="weight[]" id="">
        <button>Search</button>
    </form>
</div>