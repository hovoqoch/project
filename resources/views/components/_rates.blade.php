<button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-up"></i>
    {{$parcel->votes($type)}}
</button>
<button disabled="disabled" class="btn btn-sm"><i class="fas fa-thumbs-down"></i>
    {{$parcel->votes($type, false)}}
</button>