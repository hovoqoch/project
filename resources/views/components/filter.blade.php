<div class="filter mb-4">
    <div class="mb-3 filter__header">
        <h6 class="h6">
            {{$title}}
            <button 
                class="show btn btn-sm collapse" 
                data-toggle="collapse" 
                data-target="#section-{{$section}}">
            </button>
        </h6>
    </div>
    <div class="collapse show collapsed pl-1" id="section-{{$section}}">
        {{$slot}}
    </div>
</div>