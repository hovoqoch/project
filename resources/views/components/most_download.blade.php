<a href="{{route('parcel.show', [$parcel['id'], $parcel['thread']])}}">
    @if($parcel['image'])
    <img 
        src="{{$parcel['image']}}" 
        alt="" 
        class="card-img-top"
    >
    @else
    <div 
        class="card-img-top no-image"
    ></div>
    @endif
</a>
<div class="parcel__footer">
    <div class="div">
        <div class="float-right">
            {!! stars($parcel['rates']['avg']) !!}
            <span>({{$parcel['rates']['count']}})</span>
        </div>
        <h4 class="w-25">
            {{str_limit($parcel['number'], 6, '...')}}
        </h4>
        <h4>
            {{$parcel['tender']}}
        </h4>
    </div>
</div>
