<div class="form-group">
    <a href="{{route('auth.provider', 'facebook')}}" class="btn btn-primary btn-block">
        <i class="fab fa-facebook-f"></i>
        {{__('Facebook')}}
    </a>
</div>
<div class="form-group">
    <a href="{{route('auth.provider', 'google')}}" class="btn btn-primary btn-block">
        <i class="fab fa-google-plus-g"></i>
        {{__('Google')}}
    </a>
</div>
