<section class="mb-4">
    <div class="row">
        <div class="col-8">
            <h1 class="text-primary h2">{{__($title)}}</h1>
            <p class="text-muted">{{__($description)}}</p>
            {{$slot}}
        </div>
        <div class="col-4">
            <img src="/images/logo-header.png" class="img-fluid" alt="">
        </div>
    </div>
</section>