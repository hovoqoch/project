<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body p-5">
                <form autocomplete="off" method="POST">
                    <div class="form-group">
                        <h4 class="text-primary">{{__('Rate Parcel')}}</h4>
                        <p>
                            Your rate and review will help us <br>
                            upgrade our services.
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="">Your Rating!</label>
                        <rate :length="5" :value="1" />
                    </div>
                    <div id="rate">

                    </div>
                    @csrf
                    <input type="hidden" name="_method" value="">
                    <div class="form-group">
                        <label for="comment">Your review!</label>
                        <textarea id="comment" type="text" name="comment" rows="3" class="form-control"
                            spellcheck="false">
                        </textarea>
                    </div>
                    <input type="hidden" name="subject"
                        value="{{json_encode(['id' => $reports->first()->id, 'type' => 'parcel' ])}}">
                    <submit-button title="{{__('Submit my review')}}">
                    </submit-button>
                </form>
            </div>
        </div>
    </div>
</div>
