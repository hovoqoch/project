<?php

return [
    'reports' => '{1} :count Report|[2,*] :count Reports',
    'plans' => [
        'features' => [
            [
                'title' => 'Color Reading by Machine',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
            [
                'title' => 'Color Reading by Eye',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
            [
                'title' => 'Fluorescence',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
            [
                'title' => 'Pictures of Stone',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
            [
                'title' => 'Sarin Mapping File',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
            [
                'title' => 'Comments',
                'individual' => true,
                'tender' => true,
                'prime' => true,
            ],
        ],
        'cost' =>
        [
            [
                'title' => 'Cost per Parcel',
                'individual' => '$50',
                'tender' => 'Free',
                'prime' => 'Free',
            ],
            [
                'title' => 'Package Price',
                'individual' => 'Cross',
                'tender' => '$300',
                'prime' => '$500',
            ],
        ],
    ],
    'earn' => [
        'steps_title' => 'SafeTrade Reports in Three Steps',
        'title' => 'Earn money as a SafeTrade Member',
        'body' => 'The money you make with SafeTrade depends on when you list. reports, how many and how accurate your reports are . Find out how your fares are calculated and learn about promotions, which can help increase your earnings.',
        'cards' => [
            ['title' => 'Why use SafeTrade Reports?', 'body' => 'Earn credit to buy parcels or packages so you can view reports without any cash payment. Monetize data that you will not use anyway. Help out your peers in making important decisions and earn ratings in return. Earn real money that can be withdrawn anywhere in the world with PayPal.'],
            ['title' => 'We have your back', 'body' => 'To keep your data safe, we utilize the latest security across the website. We ensure your earnings are maintained in your SafeTrade wallet and we use PayPal to transfer money to you so you can utilize it globally*. We are available 24/7 to answer your questions or concerns.'],
        ],
        'steps' => [
            ['title' => 'Create an account', 'content' => 'Fill out a few fields and you’re ready to start.'],
            ['title' => 'List your report', 'content' => 'Click upload and fill out the form. Add images and Sarin mapping file and you’re all set. You can only upload one report per parcel, but you can upload reports for as many parcels as you like.'],
            ['title' => 'Earn immediately', 'content' => 'You earn credit based on the accuracy and upload date of the report. Click HERE to learn more.'],
        ],
        'How earnings are calculated' => [
            'title' => 'How earnings are calculated.',
            'contents' => [
                ['src' => '/images/simple_transparent.svg', 'title' => 'Simple and Transparent', 'body' => 'You will earn $50 for every report you upload. Be mindful that each parcel can only have a maximum of 5 reports after which there can be no more reports uploaded. You can only upload one report for a single parcel. There is no limit to the number of reports you can upload as long as they are for unique parcels.'],
                ['src' => '/images/peer_review.svg', 'title' => ' Peer-reviewed', 'body' => 'To encourage accurate and helpful reports, your reports will be rated by your buyers. Good reviews will increase earnings in the near future.'],
                ['src' => '/images/dynamic.svg', 'title' => 'Dynamic Pricing (Coming soon)', 'body' => 'Dynamic Pricing* will temporarily increase rewards for parcels that have no reports or lesser than average number of reports until supply and demand are rebalanced.'],
                ['src' => '/images/cash_out.svg', 'title' => 'Cash Out (Coming soon)', 'body' => 'The first upload is incredibly valuable so we will award a cash prize to the first uploader of each parcel. Details coming soon.'],
            ],
        ],
        'faq' => [
            'title' => 'Top Questions',
            'contents' => [
                ['title' => 'What if I get a negative review?', 'body' => 'Multiple negative reviews will mean we will remove your listing and deduct your credit earned. If you have already spent the credit, your credit will go into negative. You can contact us to review a removed listing and we will do a neutral review.'],
                ['title' => 'When are my credit balances updated?', 'body' => 'Your credit balances are updated immediately after every upload. You can use it immediately to buy parcels or even buy packages.'],
                ['title' => 'Do I need to wait for an approval for my reports?', 'body' => 'No, reports are live immediately after completing the upload form.'],
                ['title' => 'Who are rating my reports?', 'body' => 'Only buyers can rate your reports. In fairness, if there are multiple negative ratings, we
                can do a neutral review upon your request and clear negative ratings if we determine they are incorrect.', ],
                [
                    'title' => 'Do I need to fill out all the fields?',
                    'body' => 'The fields that are required are marked. Not each field is required, however, more information will result in better ratings. In the near future, your ratings will play an increased role in determining earnings.',
                ],
                [
                    'title' => 'Do I have to upload a Sarine machine file?',
                    'body' => 'Yes, this is a key part of each report that a Buyer will rely on. Ensure accuracy to get good ratings.',
                ],
            ],
        ],
    ],
    'buyer' => [
        'steps' => [
            ['title' => 'Create an account', 'body' => 'Fill out a few fields and you’re ready to start. You can search, buy and review from your browser.
                    Mobile apps for Reports are coming soon.', ],
            ['title' => 'Find reports', 'body' => 'Search active tenders to find reports for parcels. You can also explore by attributes such as Color
                    or Fluorescence.'],
            ['title' => 'Decide how you want to buy', 'body' => 'Purchase Prime membership, Tender Package or buy individual parcels <a href="/page/plans" role="button" class="text-dark text__underline">Learn more</a>'],
            ['title' => 'Rate the reports', 'body' => 'Let us know how helpful the report was. You can also rate the images as well as the Sarin files.'],
        ],
        'questions' => [
            ['title' => 'Do I need a subscription?', 'body' => 'You can purchase No you can buy individually any report you wish. Subscribers save money'],
            ['title' => 'What makes a good report?', 'body' => ' A good report should have accurate images, a correct Sarine mapping as well as detailed attribute reports'],
            ['title' => 'Where can I check my balance?', 'body' => 'Your points are shown at the top of the page'],
            ['title' => 'Where can I check my subscriptions?', 'body' => 'Click on your profile icon and then go to My subscriptions'],
            [
                'title' => 'What are the different packages?',
                'body' => '<ul>
<li>Monthly subscription which gives you access to everything</li>
<li>Tender package which gives access to all reports for a single tender</li>
<li>Buy credit which gives you a bonus for your purchase price (250 credit for $200 purchase). You can use the credits to buy subscriptions or individual reports. Credits don’t expire</li></ul>',
            ],
        ],
        'cards' => [
            ['src' => '/images/data_before.svg', 'title' => 'Data before you visit', 'body' => 'Plan your visit to perfection. Research parcels, review reports and view images. You won’t be wasting time'],
            ['src' => '/images/share_data.svg', 'title' => 'Share data in real-time ', 'body' => 'You can share reports with your back-office and have real-time collaboration'],
            ['src' => '/images/peace_of_mind.svg', 'title' => 'Peace of mind on every bid', 'body' => 'Bid confidently. Use expert reports with detailed analysis to eliminate doubts'],
            ['src' => '/images/save_money.svg', 'title' => 'Save money', 'body' => 'Skip traveling with planners and paying for extra hotel rooms and flights. You could even skip traveling at all and use Reports to analyze and bid on parcels from the comfort of your office'],
        ],
        'steps_title' => 'SafeTrade Reports in Four Steps',
        'cards_title' => 'Invaluable data. Always accessible.',
        'questions_title' => 'Top Questions',
    ],
];
