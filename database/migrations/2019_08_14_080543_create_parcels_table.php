<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tender_id');
            $table->string('parcel_number');
            $table->float('size')->nullable();
            $table->string('color_by_eye')->nullable();
            $table->string('comment_color_by_eye')->nullable();
            $table->string('flourscene')->nullable();
            $table->string('comment_flourscene')->nullable();
            $table->string('color_by_machine')->nullable();
            $table->string('thread')->nullable();
            $table->integer('valuation_from')->nullable();
            $table->integer('valuation_to')->nullable();
            $table->boolean('is_draft')->default(true);
            $table->integer('download')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
}
