<?php

use App\Models\Bonus;
use App\Models\Comment;
use App\Models\Parcel;
use App\Models\Post;
use App\Models\Supplier;
use App\Models\Tender;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->users(5)
            ->suppliers(5)
            ->tenders(5)
            ->parcel(50, true)
            ->parcel(50, false)
            ->bonuses();
        $this->posts();
    }

    private function users($count = 4)
    {
        factory(User::class)->create([
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
        ]);

        for ($i = 1; $i < $count; $i++) {
            factory(User::class)->create([
                'role' => 'user',
                'email' => "user{$i}@mail.com",
                'name' => "user{$i}",
                'password' => bcrypt('secret'),
            ]);
        }

        return $this;
    }

    private function suppliers($count = 10)
    {
        factory(Supplier::class, $count)->create();

        return $this;
    }

    private function tenders($count = 5)
    {
        $active = factory(Tender::class, $count)
            ->create([
                'supplier_id' => Supplier::all()->random(1)->first()->id,
                'started_at' => Carbon::now(),
                'ended_at' => Carbon::now()->addMonths(1),
            ]);

        $closed = factory(Tender::class, $count)
            ->create([
                'supplier_id' => Supplier::all()->random(1)->first()->id,
                'started_at' => Carbon::now()->addMonths(1),
                'ended_at' => Carbon::now()->addMonths(2),
            ]);

        $upcoming = factory(Tender::class, $count)
            ->create([
                'supplier_id' => Supplier::all()->random(1)->first()->id,
                'started_at' => Carbon::now()->subMonths(2),
                'ended_at' => Carbon::now()->subMonths(1),
            ]);

        return $this;
    }

    private function parcel($count = 5, $live = true)
    {
        $parcels = factory(Parcel::class, $count)->create([
            'tender_id' => Tender::active()->get()->random(1)->first()->id,
            'user_id' => User::all()->random(1)->first()->id,
            'flourscene' => $this->getRandom('fluorScenceList'),
            'color_by_eye' => $this->getRandom('colorList'),
            'color_by_machine' => $this->getRandom('colorList'),
        ]);
        if ($live) {
            $this->makeLive($parcels);
        }

        return $this;
    }

    private function getRandom($type)
    {
        $settings = settings($type);
        $key = array_rand($settings);
        return $settings[$key]['id'];
    }

    private function makeLive($parcels)
    {
        $source1 = __DIR__ . '/file.tnd';
        $source2 = __DIR__ . '/image.jpeg';
        $target1 = __DIR__ . '/target1.tnd';
        $target2 = __DIR__ . '/target2.jpeg';

        foreach ($parcels as $parcel) {
            copy($source1, $target1);
            copy($source2, $target2);

            $parcel->addMedia($target1)->toMediaCollection('files', 'media');
            $parcel->addMedia($target2)->toMediaCollection('images', 'media');
            $parcel->is_draft = false;
            $parcel->save();
        }
    }

    private function bonuses($count = 10)
    {
        foreach (User::all() as $user) {
            for ($i = 0; $i < $count; $i++) {
                $bonus = new Bonus;
                $bonus->parcel_id = 1;
                $bonus->user_id = $user->id;
                $bonus->is_active = true;
                $bonus->amount = 50;
                $bonus->save();
            }
        }

        return $this;
    }

    private function posts($count = 10)
    {
        $parcels = factory(Post::class, $count)->create([
            'tender_id' => Tender::get()->random(1)->first()->id,
            'user_id' => User::all()->random(1)->first()->id,
        ]);

        $parcels = factory(Comment::class, $count)->create([
            'post_id' => Post::get()->random(1)->first()->id,
            'user_id' => User::all()->random(1)->first()->id,
        ]);

        return $this;
    }
}
