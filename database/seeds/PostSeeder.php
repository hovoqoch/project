<?php

use App\Models\Comment;
use App\Models\Post;
use App\Models\Tender;
use App\Models\User;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();
        Comment::truncate();
        $parcels = factory(Post::class, 10)->create([
            'tender_id' => Tender::get()->random(1)->first()->id,
            'user_id' => User::all()->random(1)->first()->id,
        ]);

        $parcels = factory(Comment::class, 10)->create([
            'post_id' => Post::get()->random(1)->first()->id,
            'user_id' => User::all()->random(1)->first()->id,
        ]);

    }
}
