<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Ads;
use Faker\Generator as Faker;

$factory->define(Ads::class, function (Faker $faker) {
    return [
        'body' => $faker->text,
        'page' => '/',
        'type' => 'welcome',
        'active' => true,
    ];
});
