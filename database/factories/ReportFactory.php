<?php

use App\Models\Report;
use Faker\Generator as Faker;

$factory->define(Report::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'parcel_id' => 1,
        'title' => $faker->word,
    ];
});
