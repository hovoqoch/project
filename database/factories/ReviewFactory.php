<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Parcel;
use App\Models\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'rate' => rand(1, 5),
        'user_id' => 1,
        'comment' => $faker->text,
        'subjectable_id' => 1,
        'subjectable_type' => Parcel::class,
    ];
});
