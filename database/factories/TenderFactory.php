<?php

use App\Models\Tender;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(Tender::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'status' => $faker->word,
        'supplier_id' => 1,
        'started_at' => new \Datetime,
        'ended_at' => new \Datetime,
        'description' => $faker->text,
        'country' => Arr::random(["Angola",
            "Botswana",
            "Canada",
            "Congo",
            "Russian",
            "South Africa"]),
    ];
});
