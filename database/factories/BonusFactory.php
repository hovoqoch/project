<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Bonus;
use Faker\Generator as Faker;

$factory->define(Bonus::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'parcel_id' => 1,
        'amount' => '50',
    ];
});
