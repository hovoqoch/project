<?php

use App\Models\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'parcel_id' => 1,
        'in_wishlist' => false,
        'in_cart' => false,
    ];
});
