<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rate;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'parcel_id' => 1,
        'value' => false,
        'type' => 'report',
    ];
});
