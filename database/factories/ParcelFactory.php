<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Parcel;
use Faker\Generator as Faker;

$factory->define(Parcel::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'tender_id' => 1,
        'parcel_number' => $faker->word,
        'size' => $faker->numberBetween(10, 20),
        'color_by_eye' => settings('colorList')[0]['id'],
        'color_by_machine' => settings('colorList')[0]['id'],
        'comment_color_by_eye' => $faker->text,
        'flourscene' => settings('fluorScenceList')[0]['id'],
        'comment_flourscene' => $faker->text,
        'valuation_from' => $faker->numberBetween(10, 20),
        'valuation_to' => $faker->numberBetween(30, 40),
    ];
});
